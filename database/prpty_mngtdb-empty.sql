-- phpMyAdmin SQL Dump
-- version 4.7.6
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 29, 2017 at 09:35 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prpty_mngtdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `activity_log_id` int(11) NOT NULL,
  `property_transaction_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `activity_log_date` datetime NOT NULL,
  `activity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE `attachments` (
  `attachments_id` int(11) NOT NULL,
  `property_transaction_id` int(11) NOT NULL,
  `file` varchar(528) NOT NULL,
  `type_of_attachment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `client_id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_username` varchar(255) NOT NULL,
  `client_password` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `archive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `co_broker`
--

CREATE TABLE `co_broker` (
  `co_broker_id` int(11) NOT NULL,
  `co_broker_name` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `landline` int(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `archive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `co_broker`
--

INSERT INTO `co_broker` (`co_broker_id`, `co_broker_name`, `mobile_number`, `landline`, `email`, `address`, `archive`) VALUES
(1, 'None', '0', 0, '', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `employee_id` int(11) NOT NULL,
  `employee_name` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `employee_username` varchar(255) NOT NULL,
  `employee_password` varchar(255) NOT NULL,
  `mobile_number` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `user_type` varchar(255) NOT NULL,
  `profile_picture` varchar(528) NOT NULL,
  `archive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pictures`
--

CREATE TABLE `pictures` (
  `pictures_id` int(11) NOT NULL,
  `property_transaction_id` int(11) NOT NULL,
  `file` varchar(528) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `property_transaction`
--

CREATE TABLE `property_transaction` (
  `property_transaction_id` int(11) NOT NULL,
  `type_of_transaction` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `date` date DEFAULT NULL,
  `date_of_contract` date DEFAULT NULL,
  `end_of_contract` date DEFAULT NULL,
  `name_of_seller_id` int(11) DEFAULT NULL,
  `name_of_buyer` varchar(255) DEFAULT NULL,
  `name_of_developer` varchar(255) DEFAULT NULL,
  `developer_agent` varchar(255) DEFAULT NULL,
  `project` varchar(255) DEFAULT NULL,
  `floor_area` varchar(255) DEFAULT NULL,
  `lot_area` varchar(255) DEFAULT NULL,
  `unit_number` varchar(255) DEFAULT NULL,
  `price` varchar(255) DEFAULT NULL,
  `association_dues` varchar(255) DEFAULT NULL,
  `mobile_number` varchar(255) DEFAULT NULL,
  `landline` int(20) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `sales_person_id` int(11) NOT NULL,
  `co_broker_id` int(11) DEFAULT NULL,
  `remarks` varchar(255) DEFAULT NULL,
  `archive` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`activity_log_id`),
  ADD KEY `property_trannsation_id` (`property_transaction_id`),
  ADD KEY `employee_id` (`employee_id`);

--
-- Indexes for table `attachments`
--
ALTER TABLE `attachments`
  ADD PRIMARY KEY (`attachments_id`),
  ADD KEY `property_transaction_id` (`property_transaction_id`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `co_broker`
--
ALTER TABLE `co_broker`
  ADD PRIMARY KEY (`co_broker_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`employee_id`);

--
-- Indexes for table `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`pictures_id`),
  ADD KEY `property_transaction_id` (`property_transaction_id`);

--
-- Indexes for table `property_transaction`
--
ALTER TABLE `property_transaction`
  ADD PRIMARY KEY (`property_transaction_id`),
  ADD KEY `name_of_seller_id` (`name_of_seller_id`),
  ADD KEY `sales_person_id` (`sales_person_id`),
  ADD KEY `co_broker_id` (`co_broker_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `activity_log_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attachments`
--
ALTER TABLE `attachments`
  MODIFY `attachments_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `client`
--
ALTER TABLE `client`
  MODIFY `client_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `co_broker`
--
ALTER TABLE `co_broker`
  MODIFY `co_broker_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `employee_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pictures`
--
ALTER TABLE `pictures`
  MODIFY `pictures_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `property_transaction`
--
ALTER TABLE `property_transaction`
  MODIFY `property_transaction_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD CONSTRAINT `activity_log_ibfk_1` FOREIGN KEY (`property_transaction_id`) REFERENCES `property_transaction` (`property_transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `activity_log_ibfk_2` FOREIGN KEY (`employee_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `attachments`
--
ALTER TABLE `attachments`
  ADD CONSTRAINT `attachments_ibfk_1` FOREIGN KEY (`property_transaction_id`) REFERENCES `property_transaction` (`property_transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `pictures_ibfk_1` FOREIGN KEY (`property_transaction_id`) REFERENCES `property_transaction` (`property_transaction_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `property_transaction`
--
ALTER TABLE `property_transaction`
  ADD CONSTRAINT `property_transaction_ibfk_1` FOREIGN KEY (`name_of_seller_id`) REFERENCES `client` (`client_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `property_transaction_ibfk_2` FOREIGN KEY (`sales_person_id`) REFERENCES `employee` (`employee_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `property_transaction_ibfk_3` FOREIGN KEY (`co_broker_id`) REFERENCES `co_broker` (`co_broker_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
