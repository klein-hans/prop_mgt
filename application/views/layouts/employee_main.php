<!DOCTYPE html>
<html>
    <head>
        <title>Property Management System</title>
        <meta charset="UTF-8">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap4.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">    
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/lumino.css">

    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="">Property Management</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarColor02">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item <?php if ($this->uri->segment(2) === 'transactions') echo 'active'; ?>">
                    <a class="nav-link" href="<?php echo base_url('employee/transactions'); ?>">Transactions <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item <?php if ($this->uri->segment(2) === 'clients') echo 'active'; ?>">
                    <a class="nav-link" href="<?php echo base_url('employee/clients'); ?>">Clients <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item <?php if ($this->uri->segment(2) === 'co_brokers') echo 'active'; ?>">
                    <a class="nav-link" href="<?php echo base_url('employee/co_brokers'); ?>">Co-brokers <span class="sr-only">(current)</span></a>
                    </li>
                </ul>

                <form class="form-inline my-2 my-lg-0" wtx-context="AE38FBFF-6801-41BA-ABCF-F3EBBE95713F">
                    <?php

                    echo anchor("employee", $this->session->userdata('employee_user_type') . " " . $this->session->userdata('employee_name'), ['class' => 'btn btn-secondary']);

                    echo anchor("user/logout", 'Logout', ['class' => 'btn btn-danger']);

                    ?>
                </form>
            </div>
        </nav>

        <div class="container-fluid">
            <div class="row">
                <div class="col-2">
                    EMPLOYEE SIDEBAR
                </div>
                <div class="col-8">
                    <?php $this->load->view($employee_main); ?>
                </div>
                <div class="col-2">
                    EMPLOYEE SIDEBAR
                </div>
            </div>
        </div>

    <!--=====================
            Scripts
    =====================-->
    <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/script.js"></script>

    </body>
</html>