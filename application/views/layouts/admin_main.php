<!DOCTYPE html>
<html>
    <head>
        <title>Property Management System</title>    
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
 
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/buttons.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/responsive.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery-ui.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/lumino.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
    </head>

    <body>
         

        <!--=====================
                Top Nav
         =====================-->
        <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#sidebar-collapse"><span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span></button>
                    <img alt="image" src="<?php echo base_url(); ?>assets/images/logo.png">
                    <ul class="nav navbar-top-links navbar-right">
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                                <em class="fa fa-envelope"></em><span class="label label-danger">15</span>
                            </a>
                            <ul class="dropdown-menu dropdown-messages">
                                <li>
                                    <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                        <img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
                                        </a>
                                        <div class="message-body"><small class="pull-right">3 mins ago</small>
                                            <a href="#"><strong>John Doe</strong> commented on <strong>your photo</strong>.</a>
                                        <br /><small class="text-muted">1:24 pm - 25/03/2015</small></div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="dropdown-messages-box"><a href="profile.html" class="pull-left">
                                        <img alt="image" class="img-circle" src="http://placehold.it/40/30a5ff/fff">
                                        </a>
                                        <div class="message-body"><small class="pull-right">1 hour ago</small>
                                            <a href="#">New message from <strong>Jane Doe</strong>.</a>
                                        <br /><small class="text-muted">12:27 pm - 25/03/2015</small></div>
                                    </div>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="all-button"><a href="#">
                                        <em class="fa fa-inbox"></em> <strong>All Messages</strong>
                                    </a></div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#">
                            <em class="fa fa-bell"></em><span class="label label-info"><?php echo count($notifications); ?></span>
                            </a>
                            <ul class="dropdown-menu dropdown-alerts">
                                <?php if (count($notifications)){ ?>
                                    <?php foreach ($notifications as $notification){ ?>
                                        <li class="list-group-item d-flex justify-content-between align-items-center">
                                        <?php echo $notification->activity; ?>
                                        </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!-- /.container-fluid -->
        </nav>

        <!--=====================
                Side Nav
         =====================-->
        <div id="sidebar-collapse" class="col-sm-3 col-lg-2 sidebar">
            <div class="profile-sidebar">
                <br>
                <div class="profile-userpic">
                    <img src="http://placehold.it/50/30a5ff/fff" class="img-responsive" alt="">
                </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name">Mark Paningbatan</div>
                    <div class="profile-usertitle-status"><span class="indicator label-success"></span>Online</div>
                </div>
                <div class="clear"></div>
            </div>
            
            <ul class="nav menu">
                <li class="<?php if ($this->uri->segment(2) === 'dashboard') echo 'active'; ?>">
                    <a href="<?php echo base_url('admin/dashboard'); ?>">
                        <i class="fa fa-tachometer" aria-hidden="true"></i> Dashboard
                    </a>
                </li>
                <li class="<?php if ($this->uri->segment(2) === 'transactions') echo 'active'; ?>">
                    <a href="<?php echo base_url('employee/transactions'); ?>" >
                        <i class="fa fa-briefcase" aria-hidden="true"></i> Transactions
                    </a>
                </li>
                <li class="<?php if ($this->uri->segment(2) === 'employees') echo 'active'; ?>">
                    <a href="<?php echo base_url('admin/employees'); ?>">
                        <i class="fa fa-address-book" aria-hidden="true"></i>  Employees 
                    </a>
                </li>
                <li class="<?php if ($this->uri->segment(2) === 'clients') echo 'active'; ?>">
                    <a href="<?php echo base_url('employee/clients'); ?>">
                        <i class="fa fa-users" aria-hidden="true"></i> Clients 
                    </a>
                </li>
                <li class="<?php if ($this->uri->segment(2) === 'co_brokers') echo 'active'; ?>">
                    <a href="<?php echo base_url('employee/co_brokers'); ?>">
                        <i class="fa fa-user-circle" aria-hidden="true"></i> Co-brokers
                    </a>
                </li>

                <li class="<?php if ($this->uri->segment(2) === 'listings') echo 'active'; ?>">
                    <a href="<?php echo base_url('admin/view_listings'); ?>">
                        <i class="fa fa-list-ul" aria-hidden="true"></i> Listings
                    </a>
                </li>
                <li class="<?php if ($this->uri->segment(2) === 'activity_log') echo 'active'; ?>">
                    <a href="<?php echo base_url('admin/activity_log'); ?>">
                        <i class="fa fa-file" aria-hidden="true"></i> Activity Log 
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url('user/logout'); ?>">
                        <i class="fa fa-power-off" aria-hidden="true"></i> Logout
                    </a>
                </li>
            </ul>
        </div><!--/.sidebar-->

        <!--=====================
                Content
         =====================-->
        <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">  
            <div class="row">
                <div class="col-lg-12">
                    <?php $this->load->view($admin_main); ?>
                </div>
            </div>
        </div>            
        </div>
        <!--=====================
                Footer
         =====================-->
        <footer  id="footer" class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2 main">Copyright &copy; Affluent Properties Leasing and Sales<br> by Interns</footer>

        <!--====================
         Modal Dilog for Delete
        =====================-->   
        <div class="modal fade delete_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header"><h5>Warning!</h5></div>    
                <div class="modal-body">
                    <div class="col-xs-2"></div><div class="col-xs-8">Are you sure you want to Delete this Record?</div><div class="col-xs-2"></div>
                    <br><br><br>
                    <div class="col-xs-3"></div>
                    <div class="col-xs-6">
                        <?php 
                            $data = array(
                                'class' => 'btn btn-danger',
                                'id'    =>  'anchor_delete'
                            );
                            echo anchor("admin/", 'Yes', $data); 
                            echo " ";                    
                            $data = array(
                                    'class'         =>  'btn btn-success',
                                    'name'          => 'button',
                                    'id'            => 'button',
                                    'data-dismiss'  => 'modal'
                            );

                            echo form_button($data, "No");
                        ?>
                    </div>
                    <div class="col-xs-3"></div>
                </div>
            </div>
          </div>
        </div>
    </body>   

        <!--=====================
                Scripts
        =====================-->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script> 
        <!-- <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/transition.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/tooltip.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/popover.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/collapse.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/dropdown.js"></script>-->
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script> -->
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.min.js"></script>
        
        <script src="<?php echo base_url(); ?>assets/js/chart.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/chart-data1.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/easypiechart.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/easypiechart-data.js"></script>

        <script src="<?php echo base_url(); ?>assets/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/dataTables.bootstrap4.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/dataTables.buttons.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/buttons.flash.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jszip.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/pdfmake.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/vfs_fonts.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/buttons.html5.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/buttons.print.min.js"></script>
        
        <script src="<?php echo base_url(); ?>assets/js/script.js"></script>
        
        <script> 
            window.onload = function () {
                var chart1 = document.getElementById("line-chart").getContext("2d");
                window.myLine = new Chart(chart1).Line(lineChartData, {
                    responsive: true,
                    scaleLineColor: "rgba(0,0,0,.2)",
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    scaleFontColor: "#c5c7cc",
                    data: [20,20,20]
                });
            };
            
            /*====================
              Update Availability
            ====================*/ 
            $(document).ready(function() {
                alert("sample");    
            });
            
          // $.ajax({
          //     type: "POST", 
          //     url: <?php echo $base_url();?>+'admin/update_availability',
          //     success: function (obj) {
          //         alert("sample");
          //         if( !('error' in obj) ) {
          //             yourVariable = obj.result;
          //         }
          //         else 
          //         {
          //             console.log(obj.error);
          //         }
          //             console.log("test");

          //     }
          // });
        </script>
    
    </body>
</html>