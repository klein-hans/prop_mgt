<!DOCTYPE html>
<html>
    <head>
        <title>Property Management System</title>
        <meta charset="UTF-8">
        <title></title>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/dataTables.bootstrap4.min.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker3.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">    
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/lumino.css">
    </head>

    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <a class="navbar-brand" href="">Property Management</a>
            
            <div class="collapse navbar-collapse">

                <form class="form-inline my-2 my-lg-0" wtx-context="AE38FBFF-6801-41BA-ABCF-F3EBBE95713F">
                    <?php echo anchor("user/login_employee", 'Employee Login', ['class' => 'btn btn-primary']); ?>
                    <?php echo anchor("user/login", 'Client Login', ['class' => 'btn btn-info']); ?>
                </form>
            </div>
        </nav>
        <div class="container">
            <?php $this->load->view($main); ?>
        </div>
    </body>
</html>