<div class="panel-heading">Create Co-broker Form</div>
<div class="panel-body">
    <div class="row">
        <div class="col-lg-12">
            <?php if ($error = $this->session->flashdata('response')): ?>
                <div class="alert alert-dismissible alert-success">
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>
            <br>
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>Transaction</th>
                            <th>Employee</th>
                            <th>Date/Time</th>
                            <th>Activity</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php if (count($records)): ?>
                            <?php foreach ($records as $record): ?>
                                <tr>
                                    <td><?php echo $record->property_transaction_id; ?></td>
                                    <td><?php echo $record->employee_id; ?></td>
                                    <td><?php echo $record->activity_log_date; ?></td>
                                    <td><?php echo $record->activity; ?></td>
    							</tr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <tr>
                                <td colspan="4">No Records Found</td>
                            </tr>
                        <?php endif; ?>
                    </tbody>
                </table>

                <?php if (count($notifications)): ?>
                    <?php foreach ($notifications as $notification): ?>
                        <tr>
                            <td><?php echo $notification->property_transaction_id; ?></td>
                            <td><?php echo $notification->employee_id; ?></td>
                            <td><?php echo $notification->activity_log_date; ?></td>
                            <td><?php echo $notification->activity; ?></td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <td colspan="4">No Records Found</td>
                    </tr>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>