<br>			
<?php  
	if ($this->session->flashdata('errors')):
	        echo $this->session->flashdata('errors');
    endif;
	
	$attributes = array( 'id'	=>	'form');
	$hidden = "";
	if($process == "edit"  && $error == "false"){
		$hidden = array('id'	=> 	$records->id);
	}else if($process == "edit" && $error == "true"){
		$hidden = array('id'	=> 	$records['id']);
	}

	$action = "admin/update_bedrooms/".$process;
	echo form_open($action,"",$hidden);
?>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<?php  
						if($process == "add"){
							echo "Adding";
						}else{
							echo "Updating";
						}
						echo " Bedroom";
					?>
				</div>
				<div class="panel-body">
					<br>
				    <div class="row">
				    	<div class="col-lg-12">
				    		<label>Bedroom Name</label>	
				    	</div>
				    	<div class="col-lg-6">
				    		<?php 
			    				$data = array(
					                'class'         => 'form-control',
					                'name'          => 'name',
					            );

				    			if($error == "true"){	
									$data += array(
						                'value' => set_value('name', $records['bedroom_name'])
					            	);				    					
				    			}else if($process == "edit" && $error == "false"){
									$data += array(
						                'value' => set_value('name', $records->bedroom_name)
					            	);
				    			}
					            echo form_input($data);
				    		?>
					    </div>
					    <div class="col-lg-6">
					    	<?php 
					    		$data = array(
									'name' 	=> 'submit',
									'class' => 'btn btn-lg btn-primary',
									'value' => 'Save'
								);

								echo form_submit($data);
					    	?>
					    </div>
					    <div class="col-lg-12">
					        <?php
					        	if(form_error('name')){
					        		echo form_error('name');		
					        	}
			            	?>
			            </div>
				    </div>
				</div>
			</div>
		</div>
	</div>

<?php  
	echo form_close(); 
?>
<br><br>
