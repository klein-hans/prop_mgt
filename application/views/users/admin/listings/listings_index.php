
<Fdiv class="panel-body tabs">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab1" data-toggle="tab">Listings</a></li>
        <li><a href="#tab2" data-toggle="tab">Contacts</a></li>
        <li><a href="#tab3" data-toggle="tab">Misc</a></li>
    </ul>
    <div class="tab-content">
        <!--====================
                 Tab 1
        =====================-->   
        <div class="tab-pane fade in active" id="tab1">
            <h3>Listings</h3>
            <br>
            <!--====================
                    Alert
            =====================-->
            <?php if ($response = $this->session->flashdata('response')): ?>
                <div class="alert alert-success alert-dismissable fade in" id="success-alert"> 
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong><?php echo $response; ?></strong>
                </div>
            <?php endif; ?>

            <!--====================
                Create Button
            =====================-->
            <div class="row">
                <div class="col-lg-12">
                    <?php 
                        $process = "add";
                        echo anchor("admin/view_update_listings/{}/{$process}", '<i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Listings', ['class' => 'btn btn-primary']);
                    ?>
                </div>
            </div>
            <br><br>

            <!--====================
                    Filter
            =====================-->    
            <div class="row">
                <div class="col-lg-2 col-sm-6 col-xs-12 filter_col">
                    <br><br><br><br>
                    <div class="panel panel-info filter_panel">
                        <div class="panel-heading text-center">Filter</div>
                        <div class="panel-body">
                            <?php 
                                echo form_open('admin/view_listings');
                            ?>
                                <div class="dropdown">
                                   <button class="btn btn-sm dropdown-toggle btn_filter" type="button" data-toggle="dropdown">Property Name
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <?php 
                                            $property_name = array_unique($property_name);
                                            foreach ($property_name as $property_name) {
                                                ?>
                                                <li><input type="checkbox" name="property_name[]" value="<?=$property_name;?>"> <?=$property_name; ?></li>
                                                <?php
                                            }
                                        ?>
                                    </ul>
                                </div>
                                <div class="dropdown">
                                    <button class="btn  dropdown-toggle btn-sm btn_filter" type="button" data-toggle="dropdown">Property Type
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><input type="checkbox" name="property_type[]" value="Condo"> Condo</li>
                                        <li><input type="checkbox" name="property_type[]" value="House and Lot"> House and Lot</li>
                                    </ul>
                                </div>
                                <div class="dropdown ">
                                    <button class="btn  dropdown-toggle btn-sm btn_filter" type="button" data-toggle="dropdown">Bedroom
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <?php 
                                            foreach ($bedrooms as $key => $value) {
                                                ?>
                                                     <li><input type="checkbox" name="bedrooms[]" value="<?=$bedrooms[$key]->bedroom_name;?>"> <?=$bedrooms[$key]->bedroom_name;?></li>
                                                <?php
                                            }
                                        ?>
                                        <!-- <li><input type="checkbox" name="bedrooms[]" value="Studio"> Studio</li>
                                        <li><input type="checkbox" name="bedrooms[]" value="One Bedroom"> One Bedroom</li>
                                        <li><input type="checkbox" name="bedrooms[]" value="Two Bedroom"> Two Bedroom</li>
                                        <li><input type="checkbox" name="bedrooms[]" value="Three Bedroom"> Three Bedroom</li>
                                        <li><input type="checkbox" name="bedrooms[]" value="Four Bedroom"> Four Bedroom</li>
                                        <li><input type="checkbox" name="bedrooms[]" value="Five Bedroom"> Five Bedroom</li> -->
                                    </ul>
                                </div>    
                                <div class="dropdown ">
                                    <button class="btn  dropdown-toggle btn-sm btn_filter" type="button" data-toggle="dropdown">Parking
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                        <li><input type="checkbox" name="parking[]" value="With Parking"> With Parking</li>
                                        <li><input type="checkbox" name="parking[]" value="Without Parking"> Without Parking</li>
                                    </ul>
                                </div>
                                <div class="dropdown ">
                                    <button class="btn  dropdown-toggle btn-sm btn_filter" type="button" data-toggle="dropdown">Status
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                         <li><input type="checkbox" name="status[]" value="For Rent"> For Rent</li>
                                          <li><input type="checkbox" name="status[]" value="For Sale"> For Sale</li>
                                    </ul>
                                </div>
                                <div class="dropdown ">
                                    <button class="btn dropdown-toggle btn-sm btn_filter" type="button" data-toggle="dropdown">Availability
                                    <span class="caret"></span></button>
                                    <ul class="dropdown-menu">
                                         <li><input type="checkbox" name="availability[]" value="Available"> Available</li>
                                          <li><input type="checkbox" name="availability[]" value="Not Available"> Not Available</li>
                                    </ul>           
                                </div>     
                                <div class="btnFilter">
                                    <button class="btn btn-primary btn-sm btn_filter" type="submit" name="submit">
                                        <i class="fa fa-filter" aria-hidden="true"></i> Filter
                                    </button>
                                </div>
                                <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>

            <!--====================
                    Table
            =====================-->    

                <div class="col-lg-10">

                    <!-- <div class="table-responsive"> -->
                    <table id="listings_table" class="table table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>Property Name</th>
                                <th>Type</th>
                                <th>Bedroom</th>
                                <th>Parking</th>
                                <th>Status</th>
                                <th>Availability</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php if (count($records)){ ?>
                                <?php foreach ($records as $record){ ?>
                                <tr>
                                    <td><?php echo $record->property_name; ?></td>
                                    <td><?php echo $record->property_type; ?></td>
                                    <td><?php echo $record->bedroom_name; ?></td>
                                    <td><?php echo $record->parking_name; ?></td>
                                    <td><?php echo $record->status; ?></td>
                                    <td>
                                        <?php 
                                            if($record->availability != "Available" && $record->availability != "Not Available"){
                                                echo "Will be Available on ".$record->availability;
                                            }else{
                                                echo $record->availability;
                                            }
                                        ?>
                                    </td>
                                    <td>
                                        <?php  
                                            echo anchor("admin/view_listings_info/{$record->id}", '<i class="fa fa-info-circle" aria-hidden="true"></i> More Info', ['class' => 'btn btn-primary btn_more_info']);
                                            echo " ";
                                            $process = "edit";
                                            echo anchor("admin/view_update_listings/{$record->id}/{$process}", '<i class="fa fa-pencil" aria-hidden="true"></i> Edit', ['class' => 'btn btn-warning']);
                                            echo " ";
                                            $attrib = array(
                                                'type'          =>  'button',
                                                'class'         =>  'btn btn-danger btn_delete',
                                            );
                                            $extra = array(
                                                        'data-toggle'   =>  'modal',
                                                        'data-target'   =>  '.delete_modal',
                                                        'data-url'      =>  'delete_record/',
                                                        'data-id'       =>  $record->id,
                                                        'data-table'    =>  '/listings'
                                                        
                                                    );
                                            echo form_button($attrib, '<i class="fa fa-trash" aria-hidden="true"></i> Delete', $extra);
                                        ?>
                                    </td>
                                </tr>
                            <?php }
                            }else{ ?>
                                <tr>
                                    <td colspan="9">No Records Found</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <!--====================
                 Tab 2
        =====================-->   
        <div class="tab-pane fade" id="tab2">
            <h3>Contacts</h3>
            <br>
            <!--====================
                    Alert
            =====================-->
            <?php if ($response = $this->session->flashdata('response')): ?>
                <div class="alert alert-success alert-dismissable fade in" id="success-alert"> 
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong><?php echo $response; ?></strong>
                </div>
            <?php endif; ?>

            <!--====================
                   contact
            =====================-->   
            <div class="row">
                <div class="col-lg-6 table-responsive">
                        <table id="contacts_table" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Mobile Number</th>
                                    <th>Telephone Number</th>
                                    <th>Email Address</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if (count($records)){
                                    foreach ($records as $records){ ?>
                                        <tr>
                                            <td><?php echo $records->id; ?></td>
                                            <td><?php echo $records->owner_name; ?></td>
                                            <td><?php echo $records->mobile_number; ?></td>
                                            <td><?php echo $records->telephone_number; ?></td>
                                            <td><?php echo $records->email_address; ?></td>
                                            </td>
                                        </tr>
                                <?php }
                                }else{ ?>
                                    <tr>
                                        <td colspan="9">No Records Found</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </div>
            </div>
        </div>

        <!--====================
                 Tab 3
        =====================-->   
        <div class="tab-pane fade" id="tab3">
            <h3>Bedroom</h3>
            <br>
            <!--====================
                    Alert
            =====================-->
            <?php if ($response = $this->session->flashdata('response')): ?>
                <div class="alert alert-success alert-dismissable fade in" id="success-alert"> 
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong><?php echo $response; ?></strong>
                </div>
            <?php endif; ?>

            <!--====================
                Create Button
            =====================-->
            <div class="row">
                <div class="col-lg-12">
                    <?php 
                        $process = "add";
                        echo anchor("admin/view_update_bedrooms/{}/{$process}", '<i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Bedroom', ['class' => 'btn btn-primary']);
                    ?>
                </div>
            </div>
            <br><br>

            <!--====================
                    Table
            =====================-->   
            <div class="row">
                <div class="col-lg-6 table-responsive bedrooms_table">
                        <table id="bedrooms_table" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                if (count($bedrooms)){
                                    foreach ($bedrooms as $bedrooms){ ?>
                                        <tr>
                                            <td><?php echo $bedrooms->id; ?></td>
                                            <td><?php echo $bedrooms->bedroom_name; ?></td>
                                            <td>
                                                <?php  
                                                    $process = "edit";
                                                    echo anchor("admin/view_update_bedrooms/{$bedrooms->id}/{$process}", '<i class="fa fa-pencil" aria-hidden="true"></i> Edit', ['class' => 'btn btn-warning']);
                                                    echo " ";
                                                    $attrib = array(
                                                        'type'          =>  'button',
                                                        'class'         =>  'btn btn-danger btn_delete'
                                                    );
                                                    $extra = array(
                                                        'data-toggle'   =>  'modal',
                                                        'data-target'   =>  '.delete_modal',
                                                        'data-url'      =>  'delete_record/',
                                                        'data-id'       =>  $bedrooms->id,
                                                        'data-table'    =>  '/listings_bedrooms'
                                                        
                                                    );
                                                    echo form_button($attrib, '<i class="fa fa-trash" aria-hidden="true"></i> Delete', $extra);
                                                ?>
                                            </td>
                                    </tr>
                                <?php }
                                }else{ ?>
                                    <tr>
                                        <td colspan="9">No Records Found</td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                </div>
            </div>

            <br>
            <hr>
            <br>

            <h3>Parking</h3>
            <br>
            <!--====================
                Create Button
            =====================-->
            <div class="row">
                <div class="col-lg-12">
                    <?php 
                        $process = "add";
                        echo anchor("admin/view_update_parking/{}/{$process}", '<i class="fa fa-plus-circle" aria-hidden="true"></i> Add New Parking', ['class' => 'btn btn-primary']);
                    ?>
                </div>
            </div>
            <br><br>

            <!--====================
                    Table
            =====================-->   
            <div class="row">
                <div class="col-lg-6 table-responsive bedrooms_table">
                    <table id="parking_table" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            if (count($parking)){
                                foreach ($parking as $parking){ ?>
                                    <tr>
                                        <td><?php echo $parking->id; ?></td>
                                        <td><?php echo $parking->parking_name; ?></td>
                                        <td>
                                            <?php  
                                                $process = "edit";
                                                echo anchor("admin/view_update_parking/{$parking->id}/{$process}", '<i class="fa fa-pencil" aria-hidden="true"></i> Edit', ['class' => 'btn btn-warning']);
                                                echo " ";
                                                $attrib = array(
                                                    'type'          =>  'button',
                                                    'class'         =>  'btn btn-danger btn_delete'
                                                );
                                                $extra = array(
                                                    'data-toggle'   =>  'modal',
                                                    'data-target'   =>  '.delete_modal',
                                                    'data-url'      =>  'delete_record/',
                                                    'data-id'       =>  $parking->id,
                                                    'data-table'    =>  '/listings_parking'
                                                    
                                                );
                                                echo form_button($attrib, '<i class="fa fa-trash" aria-hidden="true"></i> Delete', $extra);
                                            ?>
                                        </td>
                                </tr>
                            <?php }
                            }else{ ?>
                                <tr>
                                    <td colspan="9">No Records Found</td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            
        </div>
    </div>
    <br><br><br>    
