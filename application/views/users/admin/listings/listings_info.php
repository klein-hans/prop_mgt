
<br>
<div class="panel panel-primary listings_info">
	<div class="panel-heading">Property Information</div>
	<div class="panel-body">
		<div class = "container-fluid" id="text">
			<div class="row">
				<div class ="col-sm-2 col-xs-6">
					<h5><b>Property Name: </b></h5>
				</div>
				<div class ="col-sm-4 col-xs-6">
					<h5>
						<?php 
						 echo $records->property_name;
						 ?>
					 </h5>
				</div>	

                <div class="col-sm-2 col-xs-6">
                    <h5><b>Floor Area: </b></h5>
                </div>
                <div class ="col-sm-4 col-xs-6">
                    <h5>
                    	<?php 
						 echo $records->floor_area;
						 ?>
                    </h5>
                </div>

				<div class="col-sm-2 col-xs-6">
					<h5><b>Property Type:</b> </h5>
				</div>
				<div class ="col-sm-4 col-xs-6">
					<h5>
						<?php 
						 echo $records->property_type;
						 ?>
					</h5>
				</div>

                <div class="col-sm-2 col-xs-6">
                    <h5><b>Lot Area: </b></h5>             
                </div>
                <div class ="col-sm-4 col-xs-6">
                    <h5>
                    	<?php 
						 echo $records->lot_area;
						 ?>
                    </h5>
                </div>

				<div class="col-sm-2 col-xs-6">
					<h5><b>Unit Number: </b></h5>
				</div>
				<div class ="col-sm-4 col-xs-6">
					<h5>
						<?php 
						 echo $records->unit_number;
						 ?>
					</h5>
				</div>

                <div class="col-sm-2 col-xs-6">
                    <h5><b>Improvements: </b></h5>
                </div>
                <div class ="col-sm-4 col-xs-6">
                    <h5>
                    	<?php 
						 echo $records->improvements;
						 ?>
                    </h5>
                </div>

				<div class="col-sm-2 col-xs-6">
					<h5><b>Price: </b></h5>
				</div> 
				<div class ="col-sm-4 col-xs-6">
					<h5>
						<?php 
						 echo $records->price;
						 ?>
					</h5>
				</div>

                <div class="col-sm-2 col-xs-6">
                    <h5><b>Parking: </b></h5>
                </div>
                <div class ="col-sm-4 col-xs-6">
                    <h5>
                    	<?php 
						 echo $records->parking_name;
						 ?>
                    </h5>
                </div>
				<div class="col-sm-2 col-xs-6">
					<h5><b>Bedroom: </b></h5>
				</div>
				<div class ="col-sm-4 col-xs-6">
					<h5>
						<?php 
						 echo $records->bedroom_name;
						?>
					</h5>
				</div>

				<div class="col-sm-2 col-xs-6">
					<h5><b>Remarks:</b> </h5>
				</div>
				<div class ="col-sm-4 col-xs-6">
					<h5> 
						<?php 
						 echo $records->remarks;
						 ?>
					</h5>
				</div>
			</div>
		</div>
		<button class="btn btn-secondary pull-right" onclick="copy()">
			<i class="fa fa-paste"></i> Copy
		</button>
 	</div>
</div>
	
<div class="panel panel-primary">
	<div class="panel-heading">Owner Information</div>
	<div class="panel-body">
		<div class = "container-fluid">
			<div class="row">
				<div class ="col-sm-2 col-xs-6">
					<h5><b>Owner Name:</b> </h5>
				</div>
				<div class ="col-sm-10 col-xs-6">
					<h5>
						<?php 
						 echo $records->owner_name;
						 ?>
					</h5>
				</div>
				<div class="col-sm-2 col-xs-6">
					<h5><b>Contact Number: </b></h5>
				</div>
				<div class ="col-sm-10 col-xs-6">
					<h5>
						<?php 
						 echo $records->mobile_number;
						 ?>
					</h5>
				</div>
				<div class="col-sm-2 col-xs-6">
					<h5><b>Telephone Number: </b></h5>
				</div>
				<div class ="col-sm-10 col-xs-6">
					<h5>
						<?php 
						 echo $records->telephone_number;
						 ?>
					</h5>
				</div>
				<div class="col-sm-2 col-xs-6">
					<h5><b>Email Address:</b> </h5>
				</div>
				<div class ="col-sm-10 col-xs-6">
					<h5>
						<?php 
						 echo $records->email_address;
						 ?>
					</h5>
				</div>
    		</div>
		</div>
	</div>
</div>


<div class="panel panel-primary listings_info image_gallery">
	<div class="panel-heading">Property Images</div>
	<div class="panel-body">
		<div class="container">
			<br><br>
		  		<div class="row images">
				  	<?php  
				  		$x = 0;
				  		foreach ($photos as $key => $val) {
			  				$src = base_url().'\uploads\photos\\'.$photos[$key]->file_name;  				
			  				$x++; 
			  				?> 
				  			<div class="col-sm-3 col-xs-12">
						      <img class="demo cursor" src="<?=$src?>" onclick="openModal();currentSlide(<?=$x;?>)" class="hover-shadow">
						    </div>
			  				<?php  
			  			}
				  	?>	
		  		</div>
		  	</div>
		  	<!-- The Modal/Lightbox -->
		<div id="lightbox" class="modal">
		 	<span class="close cursor" onclick="closeModal()">&times;</span>
			  		<?php  
			  		$x = 0;
			  		foreach ($photos as $key => $val) {
		  				$src = base_url().'\uploads\photos\\'.$photos[$key]->file_name;  				
		  				$x++; 
		  				?> 
			  			<div class="main_image">
					      <img src="<?=$src?>" >
					    </div>
		  				<?php  
			  			}
				  	?>
    		<!-- Next/previous controls -->
		  	<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    		<a class="next" onclick="plusSlides(1)">&#10095;</a>

			

					 <!-- Thumbnail image controls -->
		    
		      <div class="row thumbnail">
				  	<?php  
				  		$x = 0;
				  		foreach ($photos as $key => $val) {
			  				$src = base_url().'\uploads\photos\\'.$photos[$key]->file_name;  				
			  				$x++; 
			  				?> 
				  			<div class="col-sm-1">
						      <img class="demo cursor" src="<?=$src?>" onclick="openModal();currentSlide(<?=$x;?>)" class="hover-shadow">
						    </div>
			  				<?php  
			  			}
				  	?>	
		  		</div>
		</div>
	</div>
</div>

<div class="panel panel-primary listings_info">
	<div class="panel-heading">Property Files</div>
	<div class="panel-body">
		<br>
  		<?php  
  			$table = "listings_files";
  			foreach ($files as $key => $val) {
  				// $src = base_url().'\uploads\photos\\'.$photos[$key]->file_name;
  				?>	
	  			<div class="row">
			      	<div class="col-sm-3"><?=$files[$key]->file_name;?></div>
			      	<div class="col-sm-3">
			      		<?php  
			      			echo anchor("admin/print_file/{$files[$key]->id}/{$table}", '<i class="fa fa-download" aria-hidden="true"></i> Download', ['class' => 'btn btn-primary']);
			      		?>
			      	</div>
			  	</div>	
			  	<br>
  				<?php  
  			}
  		?>
	</div>
</div>
<br><br><br><br>	
