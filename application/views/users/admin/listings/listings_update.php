<br>
<?php  
	if ($this->session->flashdata('errors')){
	        echo $this->session->flashdata('errors');
    }
	
	$attributes = array( 'id'	=>	'form');
	$hidden = "";

	if($process == "edit"  && $error == "false"){
		$hidden = array('id'	=> 	$records->listings_id);
	}else if($process == "edit" && $error == "true"){
		$hidden = array('id'	=> 	$records['id']);
	}

	$action = "admin/update_listings/".$process;
	$attrib = array(
		'class' => "dropzone", 
		'id'	=> "my-awesome-dropzone"
	);

	echo form_open_multipart($action, $attrib, $hidden);
?>
	<div class="row listings_update">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<?php  
						if($process == "add"){
							echo "Adding";
						}else{
							echo "Updating";
						}
						echo " Listings";
					?>
				</div>
				<div class="panel-body">
				    <div class="row">
				    	<div class="col-lg-6">
				    		<label>Property Name</label>	
				    		<?php 
			    				$data = array(
					                'class'         => 'form-control',
					                'id'          	=> 'property_name',
					                'name'          => 'property_name',
					            );

				    			if($error == "true"){
									$data += array(
						                'value' => set_value('property_name', $records['property_name'])
					            	);				    					
				    			}else if($process == "edit" && $error == "false"){
									$data += array(
						                'value' => set_value('property_name', $records->property_name)
					            	);
				    			}
					            echo form_input($data);
				    		?>
					    </div>
					    <div class="col-lg-6"></div>
					    <div class="col-lg-12">
					        <?php
					        	if(form_error('property_name')){
					        		echo form_error('property_name');		
					        	}
			            	?>
			            </div>
				    </div>
				    <br>
			        
			        <div class="row">
			        	<div class="col-lg-6">
			        		<label>Property Type</label>
				            <?php 
			            		$data = array(
					                'class'         => 'form-control',
					                'id'          	=> 'property_type',
					                'name'          => 'property_type',
					            );

			    				$options = array(
					                'House and Lot'   	=> 'House and Lot',
					                'Condo'    			=> 'Condo',
					            );

			    				$selected = "";

				    			if($process == "add" && $error == "false"){
				    				$selected = "House and Lot";	
				    			}else if($process == "edit" && $error == "false"){
						        	$selected = $records->property_type;
					        	}else if($error == "true"){
					        		$selected = $records['property_type'];
					        	}
					        	echo form_dropdown('property_type', $options, $selected, $data);
				            ?>

					    </div>
					    <div class="col-lg-6"></div>
					    <div class="col-lg-12">
					        <?php
					        	if(form_error('property_type')){
					        		echo form_error('property_type');		
					        	}
			            	?>
			            </div>
			        </div>
				    <br>

				    <div class="row">
				    	<div class="col-lg-6">
				            <?php
					            echo form_label('Unit Number');
					            $data = array(
					                'class'         => 'form-control',
					                'id'          => 'unit_number',   
					                'name'          => 'unit_number'
					            );

					            if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('unit_number', $records->unit_number)
					            	);
								}else if($error == "true"){
									$data += array(
						                'value' => set_value('unit_number', $records['unit_number'])
					            	);
								}
					            echo form_input($data);
				            ?>
					    </div>
					    <div class="col-lg-6"></div>
					    <div class="col-lg-12">
					        <?php
					        	if(form_error('unit_number')){
					        		echo form_error('unit_number');		
					        	}
			            	?>
			            </div>
				    </div>
				    <br>

				    <div class="row">
				    	<div class="col-lg-6">
				            <?php
					            echo form_label('Price');

					            $data = array(
					                'class'		=> 	'form-control',
					                'id'        => 	'price',
					                'name'      => 	'price'
					            );
					            if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('price', $records->price)
					            	);
								}else if($error == "true"){
									$data += array(
						                'value' => set_value('price', $records['price'])
					            	);
								}
					            echo form_input($data);
				            ?>
					    </div>
					    <div class="col-lg-6"></div>
					    <div class="col-lg-12">
					        <?php
					        	if(form_error('price')){
					            	echo form_error('price');
					            }
				            ?>
				       	</div>
				    </div>
				    <br>
				    
				    <div class="row">
				    	<div class="col-lg-6">
				            <?php
					            echo form_label('Bedrooms');

								$data = array(
					        		'class' => 'form-control',
					        		'id'    => 'bedrooms',
					        	);
								
								$options = array();
								foreach ($bedrooms as $key => $value) {
					                $options += array(
					                     $bedrooms[$key]->id => $bedrooms[$key]->bedroom_name,
					                );
					            }

					        	$selected = "";
					        	// if($process == "edit" && $error == "false"){
					        	// 	$selected = $records->bedroom_name;	
					        	// }else 
					        	if($error == "true"){
					        		$selected = $records['bedroom_name'];	
					        	}
					        	echo form_dropdown('bedroom_name', $options, $selected, $data);
							?>
						</div>
						<div class="col-lg-6"></div>
						<div class="col-lg-12">
					        <?php
					        	if(form_error('bedroom_name')){
					            	echo form_error('bedroom_name');
					            }
				            ?>
				       	</div>
			        </div>
			        <br>
				    
			        <div class="row">
			        	<div class="col-lg-6">
				            <?php
					            echo form_label('Floor Area');

					            $data = array(
					                'class'         => 'form-control',
					                'id'          => 'floor_area',
					                'name'          => 'floor_area'
					            );

					            if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('floor_area', $records->floor_area)
					            	);
								}else if($error == "true"){
									$data += array(
						                'value' => set_value('floor_area', $records['floor_area'])
					            	);
								}
					            echo form_input($data);
				            ?>
				        </div>
				        <div class="col-lg-6"></div>
				        <div class="col-lg-12">
					        <?php
					        	if (form_error('floor_area')) {
						        	echo form_error('floor_area');
						        }
				            ?>
				       	</div>
			        </div>
			        <br>
				    
			        <div class="row">
			        	<div class="col-lg-6">
				            <?php
				            	$data = array(
					                'id'        => 'lot_area_label',
					            );
					            echo form_label('Lot Area', '', $data);

					            $data = array(
					                'class'		=> 'form-control',
					                'id'        => 'lot_area',
					                'name'      => 'lot_area'
					            );
					            if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('lot_area', $records->lot_area)
					            	);
								}else if($error == "true"){
									$data += array(
						                'value' => set_value('lot_area', $records['lot_area'])
					            	);
								}
					            echo form_input($data);
				            ?>
				        </div>
				        <div class="col-lg-6"></div>
				        <div class="col-lg-12">
					        <?php
					        	if (form_error('lot_area')) {
					        		echo form_error('lot_area');	
					        	}
				            ?>
				       	</div>
			        </div>
				    <br>
				    
				    <div class="row">
				    	<div class="col-lg-6">
				            <?php
					        	echo form_label('Improvements');

					        	$data = array(
					        		'class' => 'form-control',
					        		'id'    => 'improvements'
						        	);

					        	$options = array(
					        		'Fully Furnished' => 'Fully Furnished',
					        		'Semi-Furnished' => 'Semi-Furnished',
					        		'Unfurnished' => 'Unfurnished',
					        	);
					        	
					        	$selected = "";
					        	if($process == "add" || $error == ""){
					        		$selected = 'Fully Furnished';
					        	}else if($process == "edit" && $error == "false"){
					        		$selected = $records->improvements;	
					        	}else if($error == "true"){
					        		$selected = $records['improvements'];	
					        	}
					        	
					        	echo form_dropdown('improvements', $options, $selected, $data);
					        ?>
					   	</div>
					   	<div class="col-lg-6"></div>
					   	<div class="col-lg-12">
					   		<?php
					   		if (form_error('improvements')) {
					   			echo form_error('improvements');
					   		}
				            ?>
					   	</div>
			        </div>
			        <br>
				    
			        <div class="row">
			        	<div class="col-lg-6">
				            <?php echo form_label('Parking');

					            $data = array(
					        		'class' => 'form-control',
					        		'id'    => 'parking'
					        	);

					        	$options = array();
								foreach ($parking as $key => $value) {
					                $options += array(
					                     $parking[$key]->id => $parking[$key]->parking_name,
					                );
					            }

					        	$selected = "";
					        	// if($process == "add" && $error == "false"){
					        	// 	$selected = 'With Parking';
					        	// }else 
					        	if($process == "edit" && $error == "false"){
					        		$selected = $records->parking_name;	
					        	}else if($error == "true"){
					        		$selected = $records['parking_name'];	
					        	}
					        	
					        	echo form_dropdown('parking_name', $options, $selected, $data);
				            ?>
				        </div>
				        <div class="col-lg-6"></div>
				        <div class="col-lg-12">
				        	<?php
					        	if (form_error('parking_name')) {
					        		echo form_error('parking_name');
					        	}
				            ?>	
				        </div>
			        </div>
			        <br>
				    
				    <div class="row">
			        	<div class="col-lg-6">
				            <?php
					            echo form_label('Status');
					            $data = array(
					        		'class' => 'form-control',
					        		'id'    => 'status'
					        	);

					        	$options = array(
					        		'For Sale' => 'For Sale',
					        		'For Rent' => 'For Rent'
					        	);

					        	$selected = "";
					        	if($process == "add" && $error == "false"){
					        		$selected = 'For Sale';
					        	}else if($process == "edit" && $error == "false"){
					        		$selected = $records->status;	
					        	}else if($error == "true"){
					        		$selected = $records['status'];	
					        	}
					        	
					        	echo form_dropdown('status', $options, $selected, $data);
				            ?>
				        </div>
				        <div class="col-lg-6"></div>
				        <div class="col-lg-12">
				        	<?php
					        	if (form_error('status')) {
					        		echo form_error('status');
					        	}
				            ?>	
				        </div>
			        </div>
			        <br>

			        <div class=" row">
			        	<div class="col-lg-6">
				            <?php
					            echo form_label('Availability');
					            $data = array(
					        		'class' => 'form-control',
					        		'id'    => 'availability'
					        	);

					        	$options = array(
					        		'Not Available' => 'Not Available',
					        		'Available' => 'Available',
					        		'Will be Available on' => 'Will be Available on'
					        	);

					        	$selected = "";
					        	if($process == "edit" && $error == "false"){
					        		$selected = $records->availability;	
					        	}else if($error == "true"){
					        		$selected = $records['availability'];	
					        	}
					        	
					        	echo form_dropdown('availability', $options, $selected, $data);
				            ?>
				        </div>
				        <div class="col-lg-6 div_listing_datepicker">
				        	<br>
							<span class="fa fa-calendar" id="span_datepicker"></span><input type="text" id="datepicker" name="listing_datepicker" class="listing_datepicker">
				        </div>
				        <div class="col-lg-12">
				        	<?php
				        		if (form_error('availability')) {
				        			echo form_error('availability');
				        		}
				            ?>	
				        </div>
			        </div>
			        <br>

			        <div class="row">
				        <div class="col-lg-6">
				            <?php
					            echo form_label('Remarks');

					            $data = array(
					                'class'         => 'form-control',
					                'id'          => 'remarks',
					                'name'          => 'remarks',
					                'rows' 			=> '3'
					            );
					            if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('remarks', $records->remarks)
					            	);
								}else if($error == "true"){
					        		$data += array(
						                'value' => set_value('remarks', $records['remarks'])
					            	);
					        	}
					            echo form_textarea($data);
				         	?>
				        </div>
				        <div class="col-lg-6"></div>
				        <div class="col-lg-12">
				        	<?php
				        	if (form_error('remarks')) {
				        		echo form_error('remarks');
				        	}
				            ?>	
				        </div>
			        </div>
				</div>
			</div>
		</div>
	</div> <!-- end of row -->

	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Owner Details</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6">
				            <?php
					            echo form_label('Name');

					            $data = array(
					                'class'   => 'form-control',
					                'id'    => 'owner_name',
					                'name'    => 'owner_name'
					            );
					            
					            if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('owner_name', $records->owner_name)
					            	);
								}else if($error ==  "true"){
									$data += array(
						                'value' => set_value('owner_name', $records['owner_name'])
					            	);
								}
					            echo form_input($data);
							?>
						</div>
						<div class="col-lg-6"></div>
						<div class="col-lg-12">
							<?php
				          		if (form_error('owner_name')){
				          			echo form_error('owner_name');
				          		}
				            ?>	
						</div>
				    </div>
				    <br>
			        
			        <div class="row">
			        	<div class="col-lg-6">
				            <?php
					        	echo form_label('Mobile Number');

					        	$data = array(
					        		'class' =>  'form-control',
					        		'id'    =>  'mobile_number',
					        		'type'	=>  'text',
					        		'name'  =>  'mobile_number',
					        		'value' =>  '+639'
					        	);

					        	if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('mobile_number', $records->mobile_number)
					            	);
								}else if($error ==  "true"){
									$data += array(
						                'value' => set_value('mobile_number', $records['mobile_number'])
					            	);
								}
					        	echo form_input($data);
					        ?>
					    </div>
					    <div class="col-lg-6"></div>
					    <div class="col-lg-12">
					    	<?php
				          		if (form_error('mobile_number')) {
				          			echo form_error('mobile_number');
				          		}
				            ?>
					    </div>
			        </div>
				    <br>

				    <div class="row">
			        	<div class="col-lg-6">
				            <?php
					        	echo form_label('Telephone Number');

					        	$data = array(
					        		'class' => 'form-control',
					        		'id'    => 'telephone_number',
					        		'type'	=>	'text',
					        		'name'    => 'telephone_number'
					        	);

					        	if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('telephone_number', $records->telephone_number)
					            	);
								}else if($error ==  "true"){
									$data += array(
						                'value' => set_value('telephone_number', $records['telephone_number'])
					            	);
								}
					        	echo form_input($data);
					        ?>
					    </div>
					    <div class="col-lg-6"></div>
					    <div class="col-lg-12">
					    	<?php
				          		if (form_error('telephone_number')) {
				          			echo form_error('telephone_number');
				          		}
				            ?>
					    </div>
			        </div>
				    <br>
				    
				    <div class="row">
				    	<div class="col-lg-6">
				            <?php
					            echo form_label('Email Address');

					            $data = array(
					                'class'         => 'form-control',
					                'id'          => 'email_address',
					                'name'          => 'email_address'
					            );

					            if($process == "edit" && $error == "false"){
							    	$data += array(
						                'value' => set_value('email_address', $records->email_address)
					            	);
								}else if($error ==  "true"){
									$data += array(
						                'value' => set_value('email_address', $records['email_address'])
					            	);
								}
					            echo form_input($data);
				            ?>
				        </div>
					    <div class="col-lg-6"></div>
					    <div class="col-lg-12">
					    	<?php
					    		if (form_error('email_address')) {
					    			echo form_error('email_address');
					    		}
				            ?>
					    </div>
				    </div>
				</div>
			</div>
		</div>
	</div> <!-- end of row -->

	<div class="row listings_update">
		<div class="col-lg-12">
			<div class="panel panel-primary">
				<div class="panel-heading">Attachments</div>
				<div class="panel-body">
					<div class="col-md-10">
						<div class="form-group">
							<label>Photos</label>
						    <div class="input-group">
				                <label class="input-group-btn">
				                    <span class="btn btn-primary btn-lg">
				                        Browse&hellip; <input type="file" name="photos[]" style="display: none;" accept="image/x-png,image/gif,image/jpeg" multiple>
				                    </span>
				                </label>
				                <input type="text" class="form-control" readonly>
				            </div>
						    <i> Image file types only (eg. png, jpeg, jpg, gif).</i>
						    <br><br>
						</div>
						<?php  
							if($process == "edit"){
								?>
								<div class="form-group image_gallery">
									<div class="row">
									<?php  
							  			foreach ($photos as $key => $val) {
							  				$src = base_url().'/uploads/photos/'.$photos[$key]->file_name;
							  				
							  				?>	
							  				<div class="col-sm-3">
							  					<img src="<?=$src;?>">
							  					<br><br>
							  					<?php  
							  					$attrib = array(
		                                            'type'          =>  'button',
		                                            'class'         =>  'btn btn-danger btn_delete',
		                                        );
							  					$extra = array(
		                                                    'data-toggle'   	=>  'modal',
		                                                    'data-target'   	=>  '.delete_modal',
		                                                    'data-url'      	=>  base_url().'admin/delete_listings_file/',
		                                                    'data-listings_id'  =>  $records->listings_id.'/',
		                                                    'data-id'       	=>  $photos[$key]->id,
		                                                    'data-table'    	=>  '/listings_photos'
		                                                    
		                                                );
		                                        echo form_button($attrib, '<i class="fa fa-trash" aria-hidden="true"></i> Delete', $extra);
							  					?>
							  				</div>
							  				<?php  
							  			}
							  		?>
							  		</div>	
								</div>
								<?php
							}
						?>
						<br><br>
						<div class="form-group">
							<label>Files</label>
							<div class="input-group">
				                <label class="input-group-btn">
				                    <span class="btn btn-primary btn-lg">
				                        Browse&hellip; <input type="file" name="files[]" style="display: none;" accept=".doc,.docx,application/msword,application/pdf" multiple>
				                    </span>
				                </label>
				                <input type="text" class="form-control" readonly>
				            </div>
						    <p class="help-block">Document file types only (eg. doc, pdf).</p>
						    <br>
						</div>
						<?php  
							if($process == "edit"){
								?>
								<div class="form-group file_gallery">
									<div class="row">
									<?php  
							  			foreach ($files as $key => $val) {
							  				?>	
							  				<div class="col-sm-4"><h5>&nbsp;&nbsp;<?php echo $files[$key]->file_name;?></h5></div>
							  				<div class="col-sm-5">
							  					<?php  
							  					$attrib = array(
		                                            'type'          =>  'button',
		                                            'class'         =>  'btn btn-danger btn_delete',
		                                        );
							  					$extra = array(
		                                                    'data-toggle'   	=>  'modal',
		                                                    'data-target'   	=>  '.delete_modal',
		                                                    'data-url'      	=>  base_url().'admin/delete_listings_file/',
		                                                    'data-listings_id'  =>  $records->listings_id.'/',
		                                                    'data-id'       	=>  $files[$key]->id,
		                                                    'data-table'    	=>  '/listings_files'
		                                                    
		                                                );
		                                        echo form_button($attrib, '<i class="fa fa-trash" aria-hidden="true"></i> Delete', $extra);
							  					?>
							  					<br><br>	
							  				</div>
							  				<?php  
							  			}
							  		?>
							  		</div>	
								</div>
								<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</div> <!-- end of row -->

<?php  

	$data = array(
		'name' 	=> 'submit',
		'class' => 'btn btn-lg btn-primary',
		'value' => 'Save'
	);

	echo form_submit($data);

	echo form_close(); 

?>
<br><br><br><br>
