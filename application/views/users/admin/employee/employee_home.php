
<div class="panel-heading">Employees</div>
<div class="panel-body">
    
    <!--====================
            Alert
    =====================-->
    <div class="row">
        <div class="col-lg-12">
            <?php if ($error = $this->session->flashdata('response')): ?>
                <div class="alert alert-dismissible alert-success">
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <!--====================
        Create Button
    =====================-->
    <div class="row">
        <div class="col-lg-12">
            <?php echo anchor("admin/create_employee", 'Create', ['class' => 'btn btn-md btn-primary']); ?>
        </div>
    </div>
    <br>
    <!--====================
            Table
    =====================-->
    <div class="row">
        <div class="col-lg-12">
        <!-- <div class="table-responsive"> -->
            <table id="employee-table" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Position</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Mobile Number</th>
                        <th>Email</th>
                        <th>User Type</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($records)): ?>
                        <?php foreach ($records as $record): ?>
                            <tr>
                                <td><?php echo $record->employee_name; ?></td>
                                <td><?php echo $record->position; ?></td>
                                <td><?php echo $record->employee_username; ?></td>
                                <td><?php echo $record->employee_password; ?></td>
                                <td><?php echo $record->mobile_number; ?></td>
                                <td><?php echo $record->email; ?></td>
                                <td><?php echo $record->user_type; ?></td>
                                <td>
                                    <?php echo anchor("admin/edit_employee/{$record->employee_id}", 'Update', ['class' => 'btn btn-primary']); ?>
                                    <?php echo anchor("admin/delete_employee/{$record->employee_id}", 'Delete', ['class' => 'btn btn-danger']); ?>
                                    
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="9">No Records Found</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>