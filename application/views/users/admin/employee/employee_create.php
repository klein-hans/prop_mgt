
<div class="panel-heading">Create Employee Form</div>
<div class="panel-body">
    <?php

    $attributes = array(
        'id'    => 'create_employee_form',
        'class' => 'form_horizontal'
    );

    if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;

    echo form_open('admin/save_employee', $attributes);

    ?>
    <div class="row">
        <div class="col-lg-5">
            <?php

            echo form_label('Employee Name');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'employee_name',
                'placeholder'   => 'Enter Employee Name'
            );

            echo form_input($data);

            ?>
        </div>

        <div class="col-lg-5">
            <?php

            echo form_label('Position');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'position',
                'placeholder'   => 'Enter Position'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-5">
            <?php

            echo form_label('Employee Username');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'employee_username',
                'placeholder'   => 'Enter Employee Username'
            );

            echo form_input($data);

            ?>
        </div>

        <div class="col-lg-5">
            <?php

            echo form_label('Password');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'password',
                'placeholder'   => 'Enter password'
            );

            echo form_password($data);


            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-5">
            <?php

            echo form_label('Mobile Number');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'mobile_num',
                'placeholder'   => 'Enter Mobile Number'
            );

            echo form_input($data);

            ?>
        </div>

        <div class="col-lg-5">
            <?php

            echo form_label('Email');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'email',
                'placeholder'   => 'Enter Email'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">

        <div class="col-lg-5">
            <?php

        	echo form_label('User Type');

        	$data = array(
        		'class' => 'form-control',
        		'id'    => 'user_type'
        	);

        	$options = array(
        		'Staff' => 'Staff',
        		'Admin' => 'Admin'
        	);

        	echo form_dropdown('user_type', $options, 'Staff', $data);

        	?>
        </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-lg-5">
            <?php

            $data = array(
                'class' => 'btn btn-primary',
                'name'  => 'submit',
                'value' => 'Submit'
            );

            echo form_submit($data);

            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>