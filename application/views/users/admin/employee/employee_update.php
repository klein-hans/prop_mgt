
<div class="panel-heading">Create Employee Form</div>
<div class="panel-body">
    <?php

    $attributes = array(
        'id'    => 'create_employee_form',
        'class' => 'form_horizontal'
    );

    if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;

    echo form_open("admin/update_employee/{$records->employee_id}", $attributes);

    ?>
    <div class="row">
        <div class="col-lg-5">
            <?php

            echo form_label('Employee Name');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'employee_name',
                'value' => set_value('employee_name', $records->employee_name)
            );

            echo form_input($data);

            ?>
        </div>

        <div class="col-lg-5">
            <?php

            echo form_label('Position');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'position',
                'value' => set_value('position', $records->position)
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-5">
            <?php

            echo form_label('Employee Username');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'employee_username',
                'value' => set_value('employee_username', $records->employee_username)
            );

            echo form_input($data);

            ?>
        </div>

        <div class="col-lg-5">
            <?php

            echo form_label('Password');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'password',
                'value' => set_value('employee_password', $records->employee_password)
            );

            echo form_password($data);


            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-5">
            <?php

            echo form_label('Mobile Number');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'mobile_num',
                'value' => set_value('mobile_number', $records->mobile_number)
            );

            echo form_input($data);

            ?>
        </div>

        <div class="col-lg-5">
            <?php

            echo form_label('Email');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'email',
                'value' => set_value('email', $records->email)
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">

        <div class="col-lg-5">
            <?php

            echo form_label('User Type');

            $data = array(
                'class' => 'form-control',
                'id'    => 'user_type'
            );

            $options = array(
                'Staff' => 'Staff',
                'Admin' => 'Admin'
            );

            echo form_dropdown('user_type', $options, set_value('user_type', $records->user_type), $data);

            ?>
        </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-lg-5">
            <?php

            $data = array(
                'class' => 'btn btn-primary',
                'name'  => 'submit',
                'value' => 'Submit'
            );

            echo form_submit($data);

            ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>