

<div class="panel-heading">Co-brokers</div>
<div class="panel-body">
    
    <!--====================
            Alert
    =====================-->
    <div class="row">
        <div class="col-lg-12">
            <?php if ($error = $this->session->flashdata('response')): ?>
                <div class="alert alert-dismissible alert-success">
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <!--====================
        Create Button
    =====================-->
    <div class="row">
        <div class="col-lg-12">
            <?php echo anchor("employee/create_co_broker", 'Create', ['class' => 'btn btn-primary']); ?>
        </div>
    </div>
    <br>
    <!--====================
            Table
    =====================-->

    <div class="row">
        <div class="col-lg-12">
            
        <!-- <div class="table-responsive"> -->
        <table id="co-broker-table" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Mobile Number</th>
                    <th>Landline</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Action</th>
                    
                </tr>
            </thead>
            <tbody>
                <?php if (count($records)): ?>
                    <?php foreach ($records as $record): ?>
                        <tr>
                            <td><?php echo $record->co_broker_name; ?></td>
                            <td><?php echo $record->mobile_number; ?></td>
                            <td><?php echo $record->landline; ?></td>
                            <td><?php echo $record->email; ?></td>
                            <td><?php echo $record->address; ?></td>
                            <td><?php echo anchor("employee/edit_co_broker/{$record->co_broker_id}", 'Update', ['class' => 'btn btn-primary']); ?>
                            <?php if ($this->session->userdata('employee_user_type') === "Admin"): ?>
                                <?php echo anchor("admin/delete_co_broker/{$record->co_broker_id}", 'Delete', ['class' => 'btn btn-danger']); ?></td>
                            <?php endif; ?>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <?php if ($this->session->userdata('employee_user_type') === "Admin"): ?>
                            <td colspan="7">No Records Found</td>
                        <?php else: ?>
                            <td colspan="6">No Records Found</td>
                        <?php endif; ?>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
        <!-- </div> -->
        </div>
    </div>
</div>