

<div class="panel-heading">Create Co-broker Form</div>
<div class="panel-body">

    <?php

    $attributes = array(
        'id'    => 'create_co_broker_form',
        'class' => 'form_horizontal'
    );

    if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;

    echo form_open('employee/save_co_broker', $attributes);

    ?>

    <div class="row">
        <div class="col-lg-4">
            <?php

            echo form_label('Co-broker Name');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'co_broker_name',
                'placeholder'   => 'Enter Co-broker Name'
            );

            echo form_input($data);

            ?>
        </div>

        <div class="col-lg-4">
            <?php

            echo form_label('Email');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'email',
                'placeholder'   => 'Enter Email'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-4">
            <?php

            echo form_label('Mobile Number');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'mobile_num',
                'placeholder'   => 'Enter Mobile Number'
            );

            echo form_input($data);

            ?>
        </div>

        <div class="col-lg-4">
            <?php

            echo form_label('Landline');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'landline',
                'placeholder'   => 'Enter Landline'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-8">
            <?php

            echo form_label('Address');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'address',
                'placeholder'   => 'Enter Address'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-2">
            <?php

            $data = array(
                'class' => 'btn btn-primary',
                'name'  => 'submit',
                'value' => 'Submit'
            );

            echo form_submit($data);

            ?>
        </div>

        <?php echo form_close(); ?>