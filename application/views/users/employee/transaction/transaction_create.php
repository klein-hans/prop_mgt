
<div class="panel-heading">Create Transaction Form</div>
<div class="panel-body">
    <?php

    $attributes = array(
        'id'    => 'create_transaction_form',
        'class' => 'form_horizontal'
    );

    if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;

    echo form_open('employee/save_transaction', $attributes);

    ?>

    <div class="row">
        <div class="col-lg-5">
            <div class="form-group">
                <?php

                echo form_label('Type of Transaction');

                $data = array(
                    'class' => 'form-control',
                    'id'    => 'type_of_transaction'
                );

                $options = array(
                    'Default'       => 'Select',
                    'Pre-selling'   => 'Pre-selling',
                    'Resale'        => 'Resale',
                    'Leasing'       => 'Leasing',
                    'Daily Rental'  => 'Daily Rental'
                );

                echo form_dropdown('type_of_transaction', $options, 'Default', $data);

                ?>
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col-lg-5">
            <div class="form-group">
                <?php

                echo form_label('Category');

                $data = array(
                    'class' => 'form-control',
                    'id'    => 'category'
                );

                $options = array(
                    'Default'       => 'Select',
                    'Condominium'   => 'Condominium',
                    'House'         => 'House'
                );

                echo form_dropdown('category', $options, 'Default', $data);

                ?>
            </div>
        </div>
    </div>

    <div class="form-group pre_selling resale" hidden>
        <label class="control-label">Date</label>
        <input type="date" name="date" class="form-control" id="date" />
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group leasing daily_rental" hidden>
                <label class="control-label">Date of Contract</label>
                <input type="date" name="date_of_contract" class="form-control" id="date_of_contract" />
            </div>
        </div>

        <div class="col-6">
            <div class="form-group leasing daily_rental" hidden>
                <label class="control-label">End of Contract</label>
                <input type="date" name="end_of_contract" class="form-control" id="end_of_contract" />
            </div>
        </div>
    </div>

    <div class="form-group all" hidden>
        <label class="control-label client_seller_owner">Client/Seller/Owner</label>
        <select class="form-control" name="client_seller_owner" id="client_seller_owner">
            <?php foreach($clients as $client): ?>
                <option value="<?php echo $client->client_id; ?>"><?php echo $client->client_name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group pre_selling" hidden>
        <?php

        echo form_label('Developer');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'developer',
            'name'          => 'developer',
            'placeholder'   => 'Enter Developer Name'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group pre_selling" hidden>
        <?php

        echo form_label('Contact Person');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'contact_person',
            'name'          => 'contact_person',
            'placeholder'   => 'Enter Contact Person Name'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group house" hidden>
                <?php

                echo form_label('Lot area');

                $data = array(
                    'class'         => 'form-control',
                    'id'            => 'lot_area',
                    'name'          => 'lot_area',
                    'placeholder'   => 'Enter Lot area'
                );

                echo form_input($data);

                ?>
            </div>
        </div>

        <div class="col-12 floor_area">
            <div class="form-group all" hidden>
                <?php

                echo form_label('Floor area');

                $data = array(
                    'class'         => 'form-control',
                    'id'            => 'floor_area',
                    'name'          => 'floor_area',
                    'placeholder'   => 'Enter Floor area'
                );

                echo form_input($data);

                ?>
            </div>
        </div>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Price');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'price',
            'name'          => 'price',
            'placeholder'   => 'Enter Price'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Unit Number');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'unit_number',
            'name'          => 'unit_number',
            'placeholder'   => 'Enter Unit Number'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'buyer_tenant',
            'name'          => 'buyer_tenant',
            'placeholder'   => 'Enter Buyer Name'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant Mobile Number');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'mobile_number',
            'name'          => 'mobile_number',
            'placeholder'   => 'Enter Buyer Mobile Number'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant Landline');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'landline',
            'name'          => 'landline',
            'placeholder'   => 'Enter Buyer Landline'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant Email');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'email',
            'name'          => 'email',
            'placeholder'   => 'Enter Buyer Email'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant Address');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'address',
            'name'          => 'address',
            'placeholder'   => 'Enter Buyer Address'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <label class="control-label">Sales Agent</label>
        <select class="form-control" name="sales_agent">
            <?php foreach($employees as $employee): ?>
                <option value="<?php echo $employee->employee_id; ?>"><?php echo $employee->employee_name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group all" hidden>
        <label class="control-label">Co-broker</label>
        <select class="form-control" name="co_broker">
            <option value="1">None</option>
            <?php foreach($co_brokers as $co_broker): ?>
                <option value="<?php echo $co_broker->co_broker_id; ?>"><?php echo $co_broker->co_broker_name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Building/Project');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'building_project',
            'name'          => 'building_project',
            'placeholder'   => 'Enter Building/Project'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group leasing daily_rental" hidden>
        <?php

        echo form_label('Association Dues');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'association_dues',
            'name'          => 'association_dues',
            'placeholder'   => 'Enter Association Dues'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group leasing daily_rental" hidden>
        <?php

        echo form_label('Remarks');

        $data = array(
            'class'         => 'form-control',
            'id'            => 'remarks',
            'name'          => 'remarks',
            'placeholder'   => 'Enter Remarks'
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>

    <?php 

    echo form_label('Upload file');
    echo "<br>";
    echo form_open_multipart('upload/do_upload');?>

    <input type="file" name="userfile" size="500" />

    </div>

    <div class="form-group">
        <?php

        $data = array(
            'class' => 'btn btn-primary',
            'name'  => 'submit',
            'value' => 'Submit'
        );

        echo form_submit($data);

        ?>
    </div>

    <?php echo form_close(); ?>
</div>