
<div class="panel-heading">Update Transaction Form</div>
<div class="panel-body">

    <?php

    $attributes = array(
        'id'    => 'update_transaction_form',
        'class' => 'form_horizontal'
    );

    if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;

    echo form_open("employee/update_transaction/{$records->property_transaction_id}", $attributes);

    ?>

    <div class="row">
        <div class="col-6">
            <div class="form-group">
                <?php

                echo form_label('Type of Transaction');

                $data = array(
                    'class' => 'form-control',
                    'id'    => 'type_of_transaction'
                );

                $options = array(
                    'Default'       => 'Select',
                    'Pre-selling'   => 'Pre-selling',
                    'Resale'        => 'Resale',
                    'Leasing'       => 'Leasing',
                    'Daily Rental'  => 'Daily Rental'
                );

                echo form_dropdown('type_of_transaction', $options, set_value('type_of_transaction', $records->type_of_transaction), $data);

                ?>
            </div>
        </div>

        <div class="col-6">
            <div class="form-group">
                <?php

                echo form_label('Category');

                $data = array(
                    'class' => 'form-control',
                    'id'    => 'category'
                );

                $options = array(
                    'Default'       => 'Select',
                    'Condominium'   => 'Condominium',
                    'House'         => 'House'
                );

                echo form_dropdown('category', $options, set_value('category', $records->category), $data);

                ?>
            </div>
        </div>
    </div>

    <div class="form-group pre_selling resale" hidden>
        <label class="control-label">Date</label>
        <input type="date" name="date" class="form-control" id="date" value="<?php echo $records->date; ?>" />
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group leasing daily_rental" hidden>
                <label class="control-label">Date of Contract</label>
                <input type="date" name="date_of_contract" class="form-control" id="date_of_contract" value="<?php echo $records->date_of_contract; ?>" />
            </div>
        </div>

        <div class="col-6">
            <div class="form-group leasing daily_rental" hidden>
                <label class="control-label">End of Contract</label>
                <input type="date" name="end_of_contract" class="form-control" id="end_of_contract" value="<?php echo $records->end_of_contract; ?>" />
            </div>
        </div>
    </div>

    <div class="form-group all" hidden>
        <label class="control-label client_seller_owner">Client/Seller/Owner</label>
        <select class="form-control" name="client_seller_owner" id="client_seller_owner">
            <?php foreach($clients as $client): ?>
                <option value="<?php echo $client->client_id; ?>"
                <?php if ($client->client_id === $records->name_of_seller_id) echo 'selected'; ?>>
                <?php echo $client->client_name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group pre_selling" hidden>
        <?php

        echo form_label('Developer');

        $data = array(
            'class' => 'form-control',
            'id'    => 'developer',
            'name'  => 'developer',
            'value' => set_value('developer', $records->name_of_developer)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group pre_selling" hidden>
        <?php

        echo form_label('Contact Person');

        $data = array(
            'class' => 'form-control',
            'id'    => 'contact_person',
            'name'  => 'contact_person',
            'value' => set_value('contact_person', $records->developer_agent)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="row">
        <div class="col-6">
            <div class="form-group house" hidden>
                <?php

                echo form_label('Lot area');

                $data = array(
                    'class' => 'form-control',
                    'id'    => 'lot_area',
                    'name'  => 'lot_area',
                    'value' => set_value('lot_area', $records->lot_area)
                );

                echo form_input($data);

                ?>
            </div>
        </div>

        <div class="col-12 floor_area">
            <div class="form-group all" hidden>
                <?php

                echo form_label('Floor area');

                $data = array(
                    'class' => 'form-control',
                    'id'    => 'floor_area',
                    'name'  => 'floor_area',
                    'value' => set_value('floor_area', $records->floor_area)
                );

                echo form_input($data);

                ?>
            </div>
        </div>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Price');

        $data = array(
            'class' => 'form-control',
            'id'    => 'price',
            'name'  => 'price',
            'value' => set_value('price', $records->price)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Unit Number');

        $data = array(
            'class' => 'form-control',
            'id'    => 'unit_number',
            'name'  => 'unit_number',
            'value' => set_value('unit_number', $records->unit_number)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant');

        $data = array(
            'class' => 'form-control',
            'id'    => 'buyer_tenant',
            'name'  => 'buyer_tenant',
            'value' => set_value('buyer_tenant', $records->name_of_buyer)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant Mobile Number');

        $data = array(
            'class' => 'form-control',
            'id'    => 'mobile_number',
            'name'  => 'mobile_number',
            'value' => set_value('mobile_number', $records->mobile_number)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant Landline');

        $data = array(
            'class' => 'form-control',
            'id'    => 'landline',
            'name'  => 'landline',
            'value' => set_value('landline', $records->landline)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant Email');

        $data = array(
            'class' => 'form-control',
            'id'    => 'email',
            'name'  => 'email',
            'value' => set_value('email', $records->email)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Buyer/Tenant Address');

        $data = array(
            'class' => 'form-control',
            'id'    => 'address',
            'name'  => 'address',
            'value' => set_value('address', $records->address)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group all" hidden>
        <label class="control-label">Sales Agent</label>
        <select class="form-control" name="sales_agent">
            <?php foreach($employees as $employee): ?>
                <option value="<?php echo $employee->employee_id; ?>"
                <?php if ($employee->employee_id === $records->sales_person_id) echo 'selected'; ?>>
                <?php echo $employee->employee_name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group all" hidden>
        <label class="control-label">Co-broker</label>
        <select class="form-control" name="co_broker">
            <option value="1"
            <?php if ($records->co_broker_id === 1) echo 'selected'; ?>>
            None</option>
            <?php foreach($co_brokers as $co_broker): ?>
                <option value="<?php echo $co_broker->co_broker_id; ?>"
                <?php if ($co_broker->co_broker_id === $records->co_broker_id) echo 'selected'; ?>>
                <?php echo $co_broker->co_broker_name; ?></option>
            <?php endforeach; ?>
        </select>
    </div>

    <div class="form-group all" hidden>
        <?php

        echo form_label('Building/Project');

        $data = array(
            'class' => 'form-control',
            'id'    => 'building_project',
            'name'  => 'building_project',
            'value' => set_value('building_project', $records->project)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group leasing daily_rental" hidden>
        <?php

        echo form_label('Association Dues');

        $data = array(
            'class' => 'form-control',
            'id'    => 'association_dues',
            'name'  => 'association_dues',
            'value' => set_value('association_dues', $records->association_dues)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group leasing daily_rental" hidden>
        <?php

        echo form_label('Remarks');

        $data = array(
            'class' => 'form-control',
            'id'    => 'remarks',
            'name'  => 'remarks',
            'value' => set_value('remarks', $records->remarks)
        );

        echo form_input($data);

        ?>
    </div>

    <div class="form-group">
        <?php

        $data = array(
            'class' => 'btn btn-primary',
            'name'  => 'submit',
            'value' => 'Submit'
        );

        echo form_submit($data);

        ?>
    </div>

    <?php echo form_close(); ?>
</div>