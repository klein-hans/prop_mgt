

<div class="panel-heading">Transactions</div>
<div class="panel-body">
    <!--====================
            Alert
    =====================-->
    <div class="row">
        <div class="col-lg-12">
            <?php if ($error = $this->session->flashdata('response')): ?>
                <div class="alert alert-dismissible alert-success">
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <!--====================
        Create Button
    =====================-->
    <div class="row">
        <div class="col-lg-2">
            <?php echo anchor("employee/create_transaction", 'Create', ['class' => 'btn btn-primary']); ?>
        </div>
    </div>
    <br>
    <!--====================
            Table
    =====================-->
    <div class="row">
        <div class="col-lg-10">
            <?php echo form_open(); ?>

            <div class="row">
                <div class="col-lg-3">
                    
                    <?php

                    $data = array(
                        'class' => 'form-control',
                        'id'    => 'transaction_filter'
                    );

                    $options = array(
                        'All'           => 'All'
                    );

                    echo form_dropdown('transaction_filter', $options, 'All', $data);

                    ?>
                    
                </div>
            
                <div class="col-lg-5">
                    <?php

                    $data = array(
                        'class'         => 'form-control',
                        'id'            => 'search_transaction',
                        'name'          => 'search_transaction',
                        'placeholder'   => 'Search Transaction'
                    );

                    echo form_input($data);

                    ?>
                </div>
            
                <div class="col-lg-2">
                    <?php

                    echo form_close();

                    echo anchor("user/filter", 'Filter', ['class' => 'btn btn-lg btn-primary']);

                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="table-responsive"> -->
        <table id="transaction-table" class="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Type of Transaction</th>
                    <th>Category</th>
                    <th>Building/Project</th>
                    <th>Date</th>
                    <th>Date of Contract</th>
                    <th>End of contract</th>
                    <th>Name of Client</th>
                    <th>Name of Developer</th>
                    <th>Contact Person</th>
                    <th>Floor Area</th>
                    <th>Lot Area</th>
                    <th>Price</th>
                    <th>Unit Number</th>
                    <th>Name of Buyer</th>
                    <th>Mobile Number</th>
                    <th>Landline</th>
                    <th>Email</th>
                    <th>Address</th>
                    <th>Sales Agent</th>
                    <th>Co-broker</th>
                    <th>Association Dues</th>
                    <th>Remarks</th>
                    <th>Attachments</th>
                    <th>Pictures of Unit</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php if (count($records)): ?>
                    <?php foreach ($records as $record): ?>
                        <tr>
                            <td><?php echo $record->type_of_transaction; ?></td>
                            <td><?php echo $record->category; ?></td>
                            <td><?php echo $record->project ?></td>
                            <td><?php echo $record->date; ?></td>
                            <td><?php echo $record->date_of_contract; ?></td>
                            <td><?php echo $record->end_of_contract; ?></td>
                            <td><?php $clientid =  $record->name_of_seller_id; 
                            $query = $this->db->query("SELECT client_name FROM client WHERE client_id = $clientid;");
                            foreach ($query->result_array() as $row)
                            {
                                    echo $row['client_name'];
                            }
                            ?></td>
                            <td><?php echo $record->name_of_developer; ?></td>
                            <td><?php echo $record->developer_agent; ?></td>
                            <td><?php echo $record->floor_area; ?></td>
                            <td><?php echo $record->lot_area; ?></td>
                            <td><?php echo $record->price; ?></td>
                            <td><?php echo $record->unit_number; ?></td>
                            <td><?php echo $record->name_of_buyer; ?></td>
                            <td><?php echo $record->mobile_number; ?></td>
                            <td><?php echo $record->landline; ?></td>
                            <td><?php echo $record->email; ?></td>
                            <td><?php echo $record->address; ?></td>
                            <td><?php $sales_person =  $record->sales_person_id; 
                            $query = $this->db->query("SELECT employee_name FROM employee WHERE employee_id = $sales_person;");
                            foreach ($query->result_array() as $row)
                            {
                                    echo $row['employee_name'];
                            }
                            ?></td>
                            <td><?php $broker =  $record->co_broker_id;
                            $query = $this->db->query("SELECT co_broker_name FROM co_broker WHERE co_broker_id = $broker;");
                            foreach ($query->result_array() as $row)
                            {
                                    echo $row['co_broker_name'];
                            }
                            ?></td>
                            <td><?php echo $record->association_dues; ?></td>
                            <td><?php echo $record->remarks; ?></td>
                            <td>attachments</td>
                            <td>pictures</td>
                            <td><?php echo anchor("employee/edit_transaction/{$record->property_transaction_id}", 'Update', ['class' => 'btn btn-primary']); ?>
                            <?php if ($this->session->userdata('employee_user_type') === "Admin"): ?>
                                <?php echo anchor("admin/delete_transaction/{$record->property_transaction_id}", 'Delete', ['class' => 'btn btn-danger']); ?>
                            <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                <?php else: ?>
                    <tr>
                        <?php if ($this->session->userdata('employee_user_type') === "Admin"): ?>
                            <td colspan="26">No Records Found</td>
                        <?php else: ?>
                            <td colspan="25">No Records Found</td>
                        <?php endif; ?>
                    </tr>
                <?php endif; ?>
            </tbody>
        </table>
    <!-- </div> -->
</div>
       