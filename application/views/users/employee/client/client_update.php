

<div class="panel-heading">Update Client Form</div>
<div class="panel-body">
    <?php

    $attributes = array(
        'id'    => 'create_client_form',
        'class' => 'form_horizontal'
    );

    if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;

    echo form_open("employee/update_client/{$records->client_id}", $attributes);

    ?>

    <div class="row">
        <div class="col-lg-4">
            <?php

            echo form_label('Client Name');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'client_name',
                'value' => set_value('client_name', $records->client_name)
            );

            echo form_input($data);

            ?>
        </div>
    
        <div class="col-lg-4">
             <?php

            echo form_label('Email');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'email',
                'value' => set_value('email', $records->email)
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-4">
           <?php

            echo form_label('Username');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'client_username',
                'value' => set_value('client_username', $records->client_username)
            );

            echo form_input($data);

            ?>
        </div>
   
        <div class="col-lg-4">
            <?php

            echo form_label('Password');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'password',
                'value' => set_value('client_password', $records->client_password)
            );

            echo form_password($data);

            ?>
            
        </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-lg-4">
           <?php

            echo form_label('Gender');

            $data = array(
                'class' => 'form-control',
                'id'    => 'gender'
            );

            $options = array(
                'Female'    => 'Female',
                'Male'      => 'Male'
            );

            echo form_dropdown('gender', $options, set_value('gender', $records->gender), $data);

            ?>
        </div>
    
        <div class="col-lg-4">
            <?php

            echo form_label('Mobile Number');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'mobile_num',
                'value' => set_value('mobile_number', $records->mobile_number)
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-lg-8">
            <?php

            echo form_label('Address');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'address',
                'value' => set_value('address', $records->address)
            );

            echo form_input($data);

            ?>
            
        </div>
   </div>
    <br>
    
    <div class="row">
        <div class="col-lg-4">
            <?php

            echo form_label('Nationality');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'nationality',
                'placeholder'   => 'Enter Nationality'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-lg-4">
            <?php

            $data = array(
                'class' => 'btn btn-primary',
                'name'  => 'submit',
                'value' => 'Submit'
            );

            echo form_submit($data);

            ?>
        </div>

        <?php echo form_close(); ?>
</div>