
<div class="panel-heading">Clients</div>
<div class="panel-body">
    
    <!--====================
            Alert
    =====================-->
    <div class="row">
        <div class="col-lg-12">
            <?php if ($error = $this->session->flashdata('response')): ?>
                <div class="alert alert-dismissible alert-success">
                    <?php echo $error; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <!--====================
        Create Button
    =====================-->
    <div class="row">
        <div class="col-lg-12">
            <?php echo anchor("employee/create_client", 'Create', ['class' => 'btn btn-primary']); ?>
        </div>
    </div>
    <br>
    <!--====================
            Table
    =====================-->
    <div class="row">
        <div class="col-lg-12">  
            <!-- <div class="table-responsive"> -->
            <table id="client-table" class="table table-striped table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Username</th>
                        <th>Password</th>
                        <th>Mobile Number</th>
                        <th>Email</th>
                        <th>Address</th>
                        <th>Nationality</th>
                        <th>Gender</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if (count($records)): ?>
                        <?php foreach ($records as $record): ?>
                            <tr>
                                <td><?php echo $record->client_name; ?></td>
                                <td><?php echo $record->client_username; ?></td>
                                <td><?php echo $record->client_password; ?></td>
                                <td><?php echo $record->mobile_number; ?></td>
                                <td><?php echo $record->email; ?></td>
                                <td><?php echo $record->address; ?></td>
                                <td><?php echo $record->nationality; ?></td>
                                <td><?php echo $record->gender; ?></td>
                                <td>
                                    <?php echo anchor("employee/edit_client/{$record->client_id}", 'Update', ['class' => 'btn btn-primary']);
                                    if ($this->session->userdata('employee_user_type') === "Admin"): ?>
                                        <?php echo anchor("admin/delete_client/{$record->client_id}", 'Delete', ['class' => 'btn btn-danger']);
                                    endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <?php if ($this->session->userdata('employee_user_type') === "Admin"): ?>
                                <td colspan="10">No Records Found</td>
                            <?php else: ?>
                                <td colspan="9">No Records Found</td>
                            <?php endif; ?>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <!-- </div> -->
        </div>
    </div>
</div>