

<div class="panel-heading">Create Client Form</div>
<div class="panel-body">
    <?php

    $attributes = array(
        'id'    => 'create_client_form',
        'class' => 'form_horizontal'
    );

    if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;

    echo form_open('employee/save_client', $attributes);

    ?>

    <div class="row">
        <div class="col-lg-4">
            <?php

            echo form_label('Client Name');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'client_name',
                'placeholder'   => 'Enter Client Name'
            );

            echo form_input($data);

            ?>
        </div>
    
        <div class="col-lg-4">
             <?php

            echo form_label('Email');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'email',
                'placeholder'   => 'Enter Email'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>

    <div class="row">
        <div class="col-lg-4">
           <?php

            echo form_label('Username');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'client_username',
                'placeholder'   => 'Enter Client Username'
            );

            echo form_input($data);

            ?>
        </div>
   
        <div class="col-lg-4">
            <?php

            echo form_label('Password');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'password',
                'placeholder'   => 'Enter password'
            );

            echo form_password($data);

            ?>
            
        </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-lg-4">
           <?php

            echo form_label('Gender');

            $data = array(
                'class' => 'form-control',
                'id'    => 'gender'
            );

            $options = array(
                'Female'    => 'Female',
                'Male'      => 'Male'
            );

            echo form_dropdown('gender', $options, 'Female', $data);

            ?>
        </div>
    
        <div class="col-lg-4">
            <?php

            echo form_label('Mobile Number');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'mobile_num',
                'placeholder'   => 'Enter Mobile Number'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-lg-8">
            <?php

            echo form_label('Address');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'address',
                'placeholder'   => 'Enter Address'
            );

            echo form_input($data);

            ?>
            
        </div>
   </div>
    <br>
    
    <div class="row">
        <div class="col-lg-4">
            <?php

            echo form_label('Nationality');

            $data = array(
                'class'         => 'form-control',
                'name'          => 'nationality',
                'placeholder'   => 'Enter Nationality'
            );

            echo form_input($data);

            ?>
        </div>
    </div>
    <br>
    
    <div class="row">
        <div class="col-lg-4">
            <?php

            $data = array(
                'class' => 'btn btn-primary',
                'name'  => 'submit',
                'value' => 'Submit'
            );

            echo form_submit($data);

            ?>
        </div>

        <?php echo form_close(); ?>
</div>