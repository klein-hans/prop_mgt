<p class="bg-danger">

<?php
    if ($this->session->flashdata('no_access')):
        echo $this->session->flashdata('no_access');
    endif;
?>

</p>

<div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
        <img src="<?php echo base_url(); ?>assets/images/affluent_logo.png" style="width: 100%">
    </div>
</div>

<div class="row">
    <div class="col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4">
        <div class="login-panel panel panel-default">
            <div class="panel-heading">Log in</div>
            <div class="panel-body">
                <?php
                    $attributes = array(
                        'id'    => 'login_form',
                        'class' => 'form_horizontal'
                    );

                    if ($this->session->flashdata('errors')):
                        echo $this->session->flashdata('errors');
                    endif;

                    echo form_open('user/employee_login', $attributes);
                ?>

                <div class="form-group">
                    <?php

                    $data = array(
                        'class'         => 'form-control',
                        'name'          => 'username',
                        'placeholder'   => 'Username',
                        'autofocus'     =>  ''
                    );
 
                    echo form_input($data);

                    ?>
                </div>

                <div class="form-group">
                    <?php

                    $data = array(
                        'class'         => 'form-control',
                        'name'          => 'password',
                        'placeholder'   => 'Password'
                    );

                    echo form_password($data);

                    ?>
                </div>

                <div class="form-group">
                    <?php

                    $data = array(
                        'class' => 'btn btn-primary',
                        'name'  => 'submit',
                        'value' => 'Login'
                    );

                    echo form_submit($data);

                    ?>
                </div>

                <?php echo form_close(); ?>

            </div>
        </div>
    </div><!-- /.col-->
</div><!-- /.row -->    
    
