<p class="bg-danger">

<?php
    if ($this->session->flashdata('no_access')):
        echo $this->session->flashdata('no_access');
    endif;
?>

</p>

<h2>Login form</h2>

<?php
    $attributes = array(
        'id'    => 'login_form',
        'class' => 'form_horizontal'
    );

    if ($this->session->flashdata('errors')):
        echo $this->session->flashdata('errors');
    endif;

    echo form_open('user/client_login', $attributes);
?>

<div class="form-group">
    <?php

    echo form_label('Username');

    $data = array(
        'class'         => 'form-control',
        'name'          => 'username',
        'placeholder'   => 'Enter username'
    );

    echo form_input($data);

    ?>
</div>

<div class="form-group">
    <?php

    echo form_label('Password');

    $data = array(
        'class'         => 'form-control',
        'name'          => 'password',
        'placeholder'   => 'Enter password'
    );

    echo form_password($data);

    ?>
</div>

<div class="form-group">
    <?php

    $data = array(
        'class' => 'btn btn-primary',
        'name'  => 'submit',
        'value' => 'Login'
    );

    echo form_submit($data);

    ?>
</div>

<?php echo form_close(); ?>
