<?php

class Client extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('logged_in'))
        {
            $this->session->set_flashdata('no_access', 'Sorry you are not allowed');
            redirect('user');
        }
    }

    public function index()
    {

    }
}