<?php

class User extends CI_Controller {

    public function index()
    {
        $data['main'] = "users/home";
        $this->load->view('layouts/main', $data);
    }

    // CLIENT
    public function login()
    {
        $data['main'] = "users/client/login";
        $this->load->view('layouts/main', $data);
    }

    public function client_login()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');

        if ($this->form_validation->run() === FALSE)
        {
            $data = array(
                'errors' => validation_errors()
            );

            $this->session->set_flashdata($data);
            redirect('user/login');
        }
        else
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user_data_db = $this->user_model->login_user($username, $password, 'client');

            if ($user_data_db)
            {
                $user_data = array(
                    'client_id'       => $user_id,
                    'client_username' => $username,
                    'logged_in'       => TRUE
                );

                $this->session->set_userdata($user_data);

                redirect('client');
            }

            redirect('user/login');
        }
    }
    // END OF CLIENT

    // EMPLOYEE
    public function employee_login()
    {
        $this->form_validation->set_rules('username', 'Username', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[3]');

        if ($this->form_validation->run() === FALSE)
        {
            $data = array(
                'errors' => validation_errors()
            );

            $this->session->set_flashdata($data);
            redirect('user/login_employee');
        }
        else
        {
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $user_data_db = $this->user_model->login_user($username, $password, 'employee');

            if ($user_data_db)
            {
                foreach ($user_data_db as $user)
                {
                    $user_data = array(
                        'employee_id'               => $user->employee_id,
                        'employee_name'             => $user->employee_name,
                        'employee_position'         => $user->position,
                        'employee_username'         => $user->employee_username,
                        'employee_password'         => $user->employee_password,
                        'employee_mobile_number'    => $user->mobile_number,
                        'employee_email'            => $user->email,
                        'employee_profile_picture'  => $user->profile_picture,
                        'employee_user_type'        => $user->user_type,
                        'logged_in'                 => TRUE
                    );
                }

                $this->session->set_userdata($user_data);

                if ($this->session->userdata('employee_user_type') === "Admin")
                {
                    redirect('admin');
                }

                redirect('employee');
            }

            redirect('user/login_employee');
        }
    }

    public function login_employee()
    {
        $data['main'] = "users/employee/login";
        $this->load->view('layouts/main', $data);
    }
    // END OF EMPLOYEE

    public function logout()
    {
        $this->session->sess_destroy();

        redirect('user');
    }
}