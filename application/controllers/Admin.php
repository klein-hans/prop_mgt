<?php

class Admin extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('logged_in'))
        {
            $this->session->set_flashdata('no_access', 'Sorry you are not allowed');
            redirect('user');
        }
    }

    public function index()
    {
        redirect('employee/transactions');
    }   

    public function dashboard(){

            $data['notifications'] = $this->admin_model->get_unread_activity_records();
            $data['admin_main'] = 'users/admin/dashboard';
            $this->load->view('layouts/admin_main', $data);
    }
    // EMPLOYEE
    public function employees()
    {
        if ($this->session->userdata('employee_user_type') !== "Admin")
        {
            redirect('employee');
        }
        else
        {
            $data['notifications'] = $this->admin_model->get_unread_activity_records();
            $data['records'] = $this->admin_model->get_employee_records();
            $data['admin_main'] = 'users/admin/employee/employee_home';
            $this->load->view('layouts/admin_main', $data);
        }
    }

    public function create_employee()
    {
        if ($this->session->userdata('employee_user_type') !== "Admin")
        {
            redirect('employee');
        }
        else
        {
            $data['notifications'] = $this->admin_model->get_unread_activity_records();
            $data['admin_main'] = 'users/admin/employee/employee_create';
            $this->load->view('layouts/admin_main', $data);
        }
    }

    public function save_employee()
    {
        $this->form_validation->set_rules('employee_name', 'Employee Name', 'required');
        $this->form_validation->set_rules('position', 'Position', 'required');
        $this->form_validation->set_rules('employee_username', 'Employee Username', 'required');
        $this->form_validation->set_rules('password', 'Employee Password', 'required');
        $this->form_validation->set_rules('mobile_num', 'Mobile Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('user_type', 'User Type', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() === TRUE)
        {
            $data = array(
                'employee_name'     => $_POST['employee_name'],
                'position'          => $_POST['position'],
                'employee_username' => $_POST['employee_username'],
                'employee_password' => $_POST['password'],
                'mobile_number'     => $_POST['mobile_num'],
                'email'             => $_POST['email'],
                'user_type'         => $_POST['user_type'],
                'archive'           => 0
            );

            if ($this->admin_model->save_employee_record($data))
            {
                $this->session->set_flashdata('response', 'Record Save Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Save Record!');
            }

            redirect('admin/employees');
        }
        else
        {
            redirect('admin/employees');
        }
    }

    public function edit_employee($record_id)
    {
        if ($this->session->userdata('employee_user_type') !== "Admin")
        {
            redirect('employee');
        }
        else
        {
            $data['notifications'] = $this->admin_model->get_unread_activity_records();
            $data['records'] = $this->admin_model->get_employee_record($record_id);
            $data['admin_main'] = 'users/admin/employee/employee_update';
            $this->load->view('layouts/admin_main', $data);
        }
    }

    public function update_employee($record_id)
    {
        $this->form_validation->set_rules('employee_name', 'Employee Name', 'required');
        $this->form_validation->set_rules('position', 'Position', 'required');
        $this->form_validation->set_rules('employee_username', 'Employee Username', 'required');
        $this->form_validation->set_rules('password', 'Employee Password', 'required');
        $this->form_validation->set_rules('mobile_num', 'Mobile Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('user_type', 'User Type', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() === TRUE)
        {
            $data = array(
                'employee_name'     => $_POST['employee_name'],
                'position'          => $_POST['position'],
                'employee_username' => $_POST['employee_username'],
                'employee_password' => $_POST['password'],
                'mobile_number'     => $_POST['mobile_num'],
                'email'             => $_POST['email'],
                'user_type'         => $_POST['user_type']
            );

            if ($this->session->userdata('employee_id') === $record_id) {
                $user_data = array(
                    'employee_name' => $_POST['employee_name'],
                    'employee_position' => $_POST['position'],
                    'employee_username' => $_POST['employee_username'],
                    'employee_password' => $_POST['password'],
                    'employee_mobile_number' => $_POST['mobile_num'],
                    'employee_email' => $_POST['email'],
                    'employee_user_type' => $_POST['user_type'],
                );

                $this->session->set_userdata($user_data);
            }

            if ($this->admin_model->update_employee_record($record_id, $data))
            {
                $this->session->set_flashdata('response', 'Updated Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Update Record!');
            }

            redirect('admin/employees');
        }
        else
        {
            redirect('admin/employees');
        }
    }

    public function delete_employee($record_id)
    {
        if ($this->session->userdata('employee_user_type') !== "Admin")
        {
            redirect('employee');
        }
        else
        {
            $data = array(
                'archive' => 1
            );

            if ($this->admin_model->delete_employee_record($record_id, $data))
            {
                $this->session->set_flashdata('response', 'Record Deleted Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Delete Record!');
            }

            redirect('admin/employees');
        }
    }
    // END OF EMPLOYEE

    // CLIENT
    public function delete_client($record_id)
    {
        if ($this->session->userdata('employee_user_type') !== "Admin")
        {
            redirect('employee/clients');
        }
        else
        {
            $data = array(
                'archive' => 1
            );

            if ($this->admin_model->delete_client_record($record_id, $data))
            {
                $this->session->set_flashdata('response', 'Record Deleted Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Delete Record!');
            }

            redirect('employee/clients');
        }
    }
    // END OF CLIENT

    // CO_BROKER
    public function delete_co_broker($record_id)
    {
        if ($this->session->userdata('employee_user_type') !== "Admin")
        {
            redirect('employee/co_brokers');
        }
        else
        {
            $data = array(
                'archive' => 1
            );

            if ($this->admin_model->delete_co_broker_record($record_id, $data))
            {
                $this->session->set_flashdata('response', 'Record Deleted Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Delete Record!');
            }

            redirect('employee/co_brokers');
        }
    }
    // END OF CO_BROKER

    // PROPERTY TRANSACTION
    public function delete_transaction($record_id)
    {
        if ($this->session->userdata('employee_user_type') !== "Admin")
        {
            redirect('employee/transactions');
        }
        else
        {
            $data = array(
                'archive' => 1
            );

            if ($this->admin_model->delete_transaction_record($record_id, $data))
            {
                $this->session->set_flashdata('response', 'Record Deleted Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Delete Record!');
            }

            redirect('employee/transactions');
        }
    }
    // END OF PROPERTY TRANSACTION
  // ACTIVITY LOG
    public function activity_log()
    {
        if ($this->session->userdata('employee_user_type') !== 'Admin')
        {
            redirect('employee');
        }
        else
        {
            $data['notifications'] = $this->admin_model->get_unread_activity_records();
            $data['records'] = $this->admin_model->get_activity_log_records();
            $data['admin_main'] = 'users/admin/activity_log';
            $this->load->view('layouts/admin_main', $data);
        }
    }
    // END OF ACTIVITY LOG


    /*====================
            Listings
    ====================*/
    public function view_listings(){
        
        if (isset($_POST['submit'])) {
            unset($_POST['submit']);
        }
        $data['records'] = $this->admin_model->get_listings_records($_POST);
        $data['bedrooms'] = $this->admin_model->get_bedrooms();
        $data['parking'] = $this->admin_model->get_parking();
        $data['property_name'] = array();
        $listings = $this->admin_model->get_record(array('archive' => 0), "listings");
        foreach ($listings as $key => $object) {
            array_push($data['property_name'], $listings[$key]->property_name);
        }
        /*=========For Property Name===========*/
        
        foreach ($data['records'] as $key => $object) {
            
            date_default_timezone_set("Asia/Manila");
            $date_today =  Date("m/d/Y");
            if($date_today == $data['records'][$key]->availability){
                $data['availability'] = array(
                    'id'            => $data['records'][$key]->id,
                    'availability'  => "Available",
                );    
                $this->admin_model->edit_listings_record($data['availability']);    
            }
        }
        sort($data['property_name']);
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['admin_main'] = 'users/admin/listings/listings_index';
        $this->load->view('layouts/admin_main', $data);
    }

    public function view_listings_info($id){
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['admin_main'] = 'users/admin/listings/listings_info';
        $data['records'] = $this->admin_model->get_listings_record($id);
        $data['photos'] = $this->admin_model->get_listings_photos($id);
        $data['files'] = $this->admin_model->get_listings_files($id);
        $this->load->view('layouts/admin_main', $data);
    }

    public function view_update_listings($id, $process){
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['bedrooms'] = $this->admin_model->get_bedrooms();
        $data['parking'] = $this->admin_model->get_parking();
        $data['admin_main'] = 'users/admin/listings/listings_update';
        $data['process'] = $process;
        $data['error'] = "false";
        $data['records'] = "";
        if($process == "edit"){
            $data['records'] = $this->admin_model->get_listings_record($id);
            $data['photos'] = $this->admin_model->get_listings_photos($id);
            $data['files'] = $this->admin_model->get_listings_files($id);
        }   
        $this->load->view('layouts/admin_main', $data);
    }

    public function update_listings($process){
        if($_POST['availability'] == "Will be Available on"){
            $_POST['availability'] = $_POST['listing_datepicker'];
        }
        unset($_POST['listing_datepicker']);
        
        $bedroom = $this->admin_model->get_bedroom_record($_POST['bedroom_name']);
        $parking = $this->admin_model->get_parking_record($_POST['parking_name']);
        $_POST['bedroom_name'] = $bedroom->bedroom_name;
        $_POST['parking_name'] = $parking->parking_name;
        
        $valid = $this->custom_validation();

        if($valid == "false"){
            $data['notifications'] = $this->admin_model->get_unread_activity_records();
            $data['bedrooms'] = $this->admin_model->get_bedrooms();
            $data['parking'] = $this->admin_model->get_parking();
            $data['admin_main'] = 'users/admin/listings/listings_update';
            $data['error'] = "true";
            $data['process'] = $process;
            $data['records'] = array();
            $_POST['bedroom_name'] = $bedroom->id;
            $_POST['parking_name'] = $parking->id;

            foreach ($_POST as $key => $value) 
            {
                $data['records'] += array(
                     $key => $value,
                );
            }
            $this->load->view('layouts/admin_main', $data);
        }
        else
        {   
            unset($_POST['bedroom_name']);
            unset($_POST['parking_name']);
            $_POST['bedroom_id'] = $bedroom->id;
            $_POST['parking_id'] = $parking->id;
            $id = "";
            if($process == "add"){
                $id = $this->admin_model->add_listings_record($_POST);
            }else if($process == "edit"){   
                $id = $this->admin_model->edit_listings_record($_POST);
            }
            var_dump($_POST);
            exit();
            if ($id){
                $this->upload_file($id);
                $this->session->set_flashdata('m_responsekeys(conn, identifier)', 'Listings Save Successfully.');
            }else{
                $this->session->set_flashdata('response', 'Failed to Save Listings!');
            }
            redirect('admin/view_listings');
        }
    }

    public function delete_listings($id){
        $data = array(
            'archive' => 1
        );
        if ($this->admin_model->delete_listings_record($id, $data)){
            $this->session->set_flashdata('response', 'Record Deleted Successfully.');
        }else{
            $this->session->set_flashdata('response', 'Failed to Delete Record!');
        }

        redirect('admin/view_listings');
    }

    public function delete_listings_file($listings_id, $id, $table){
        $data = array(
            'archive' => 1
        );
        $rows = $this->admin_model->delete_permanent($table, $id);
        unlink($_SERVER['DOCUMENT_ROOT']."/prop_mgt/".$rows[0]->file_path);
        $this->view_update_listings($id, "edit");
        $this->session->set_flashdata('response', 'Record Deleted Successfully.');
        redirect('admin/view_update_listings/'.$listings_id.'/edit');
    }

    public function update_availability(){
        $query = $this->admin_model->get_records("listings");
        echo "hey";
        print_r("hey");
        var_dump("huy");
        exit();
        foreach ($query->result() as $row)
        {
            echo $row->availability;
        }
    }

    /*====================
            Bedrooms
    ====================*/
    public function view_update_bedrooms($id, $process){
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['bedrooms'] = $this->admin_model->get_bedrooms();
        $data['admin_main'] = 'users/admin/listings/bedrooms_update';
        $data['process'] = $process;
        $data['error'] = "false";
        if($process == "edit"){
            $data['records'] = $this->admin_model->get_bedroom_record($id);
        }   
        $this->load->view('layouts/admin_main', $data);
    }
    
    public function update_bedrooms($process){
        $valid = $this->custom_validation();
        if($valid == "false"){
            $data['notifications'] = $this->admin_model->get_unread_activity_records();
            $data['admin_main'] = 'users/admin/listings/bedrooms_update';
            $data['error'] = "true";
            $data['process'] = $process;
            $data['records'] = $_POST;
            $arr = array();
            foreach ($_POST as $key => $value) {
                $arr += array(
                     $key => $value,
                );
            }
            array_push($data['records'], $arr);
            $this->load->view('layouts/admin_main', $data);
        }else{
            if($process == "add"){
                if ($this->admin_model->add_record($_POST, "listings_bedrooms")){
                    $this->session->set_flashdata('response', 'Bedrooms Save Successfully.');
                }else{
                    $this->session->set_flashdata('response', 'Failed to Save Bedrooms!');
                }
            }else if($process == "edit"){    
                if($this->admin_model->edit_record($_POST, "listings_bedrooms")){
                    $this->session->set_flashdata('response', 'Bedrooms Save Successfully.');

                }else{
                    $this->session->set_flashdata('response', 'Failed to Save Bedrooms!');
                }   
           }
            redirect('admin/view_listings');
        }
        
    }

    /*==================
          Parking
    ==================*/ 
    public function view_update_parking($id, $process){
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['parking'] = $this->admin_model->get_parking();
        $data['admin_main'] = 'users/admin/listings/parking_update';
        $data['process'] = $process;
        $data['error'] = "false";
        if($process == "edit"){
            $data['records'] = $this->admin_model->get_parking_record($id);
        }   
        $this->load->view('layouts/admin_main', $data);
    }

    public function update_parking($process){
        $valid = $this->custom_validation();
        if($valid == "false"){
            $data['notifications'] = $this->admin_model->get_unread_activity_records();
            $data['admin_main'] = 'users/admin/listings/parking_update';
            $data['error'] = "true";
            $data['process'] = $process;
            $data['records'] = $_POST;
            $arr = array();
            foreach ($_POST as $key => $value) {
                $arr += array(
                     $key => $value,
                );
            }
            array_push($data['records'], $arr);
            $this->load->view('layouts/admin_main', $data);
        }else{
            if($process == "add"){
                if ($this->admin_model->add_record($_POST, "listings_parking")){
                    $this->session->set_flashdata('response', 'Parking Save Successfully.');
                }else{
                    $this->session->set_flashdata('response', 'Failed to Save Bedrooms!');
                }
            }else if($process == "edit"){    
                if($this->admin_model->edit_record($_POST, "listings_parking")){
                    $this->session->set_flashdata('response', 'Parking Save Successfully.');
                }else{
                    $this->session->set_flashdata('response', 'Failed to Save Bedrooms!');
                }   
           }
            redirect('admin/view_listings');
        }
        
    }

    public function delete_record($id, $table){
        $data = array(
            'archive' => 1
        );

        if($table == "listings_photos"){
            $rows = $this->admin_model->delete_permanent($table, $id);
            unlink($_SERVER['DOCUMENT_ROOT']."/prop_mgt/".$rows[0]->file_path);
            $this->view_update_listings($id, "edit");
            redirect('admin/view_update_listings/'.$id.'/edit');
        }else{
            if ($this->admin_model->delete_record($table, $id, $data)){
                $this->session->set_flashdata('response', 'Record Deleted Successfully.');
            }else{
                $this->session->set_flashdata('response', 'Failed to Delete Record!');
            }
            redirect('admin/view_listings');
        }
    }

    /*==================
        Validation
    ==================*/ 
    public function custom_validation(){
        $valid = true;
        $config = array();
        unset($_POST['submit']);
        
        if($_POST['property_type'] == "Condo"){
            $_POST['lot_area'] = "None";
        }

        foreach (array_keys($_POST) as $field){
            $label = str_replace("_", " ", $field);
            $label = ucwords($label);
            $arr = array(
                'field' => $field,
                'label' => $label,
                'rules' =>  'required'
            );        

            if($field == "id"){
                $arr = array(
                    'field' => $field,
                    'label' => $label,
                    'rules' =>  'required'
                );        
            }

            array_push($config, $arr);
        }
            
        $this->form_validation->set_rules($config);       
        $this->form_validation->set_error_delimiters(
            '<p style="color:Red; font-size:12px"><?php echo validation_errors();?>',' </p>'
        );
        
        if ($this->form_validation->run() === false){
            $valid = false;
        }
        
        if($valid == true){
            return "true";   
        }else if($valid == false){
            return "false";   
        }
    }

    /*==================
        Upload Files
    ==================*/ 
    public function upload_file($id)
    {
        $config['allowed_types']        = 'gif|jpg|png|pdf|doc|docx';
        $config['max_size']             = 2048;
        $config['max_width']            = 2000;
        $config['max_height']           = 1000;

        $this->load->library('upload', $config);
        $x = 0;
        foreach ($_FILES['photos']['name'] as $key => $value) 
        {   
            echo "number ".$x++." ";
            $config['upload_path']          = './uploads/photos';
            $config['file_name'] = $_FILES['photos']['name'][$key];
            $this->upload->initialize($config); 
            var_dump($_FILES['photos']['name'][$key]);
            $_FILES['photos[]']['name'] = $_FILES['photos']['name'][$key];
            $_FILES['photos[]']['type'] = $_FILES['photos']['type'][$key];
            $_FILES['photos[]']['tmp_name'] = $_FILES['photos']['tmp_name'][$key];
            $_FILES['photos[]']['size'] = $_FILES['photos']['size'][$key];

            if ($this->upload->do_upload('photos[]'))
            {
                $path = "\uploads\photos\\".basename($_FILES['photos']['name'][$key]);  
                $name = $_FILES['photos']['name'][$key];
                $data = array( 
                    'listings_id'   =>  $id,
                    'file_name'     =>  $name,
                    'file_path'     =>  $path,
                );
                $this->admin_model->add_files($data, "listings_photos");   
            }
            else
            {
                echo $this->upload->display_errors();
                $path = "\uploads\photos\\".basename($_FILES['photos']['name'][$key]);  
            }
        }
        foreach ($_FILES['files']['name'] as $key => $value) 
        {   
            echo $x++;
            $config['upload_path']          = './uploads/files';
            $config['file_name'] = $_FILES['files']['name'][$key];
            $this->upload->initialize($config);
            $_FILES['files[]']['name'] = $_FILES['files']['name'][$key];
            $_FILES['files[]']['type'] = $_FILES['files']['type'][$key];
            $_FILES['files[]']['tmp_name'] = $_FILES['files']['tmp_name'][$key];
            $_FILES['files[]']['size'] = $_FILES['files']['size'][$key];

            if (! $this->upload->do_upload('files[]'))
            {
                echo $this->upload->display_errors();
                $path = "\uploads\\files\\".basename($_FILES['files']['name'][$key]);  
            }
            else
            {
                $path = "\uploads\\files\\".basename($_FILES['files']['name'][$key]);  
                $name = $_FILES['files']['name'][$key];
                $data = array( 
                    'listings_id'   =>  (int)$id,
                    'file_name'     =>  $name,
                    'file_path'     =>  $path,
                );
                $this->admin_model->add_files($data, "listings_files");   
            }
        }           
        // foreach ($file['name'] as $key => $image) {
        //     $_FILES['userfile[]']['name'] = $file['name'][$key];
        //     $_FILES['userfile[]']['type'] = $file['type'][$key];
        //     $_FILES['userfile[]']['tmp_name'] = $file['tmp_name'][$key];
        //     $_FILES['userfile[]']['error'] = $file['error'][$key];
        //     $_FILES['userfile[]']['size'] = $file['size'][$key];

        //     $fileName = $image;
        //     $userfile[] = $fileName;
            

        //     if ($this->upload->do_upload('userfile[]'))
        //     {
        //         $data_file = $this->upload->data();

        //         $data = array(
        //             'transaction_id'=> $id,
        //             'type'          => $attachment[$key],
        //             'file'          =>  $data_file['file_name']
        //         );

        //         $this->employee_model->save_attachments($data);
        //     }
        // }
    }

    public function print_file($id, $table)
    {
        $this->load->helper('download');
        $where = array(
            'id'    =>  $id
        );
        $rows = $this->admin_model->get_record($where, $table);
        $data = file_get_contents ($_SERVER['DOCUMENT_ROOT']."/prop_mgt".$rows[0]->file_path);
        force_download($rows[0]->file_name, $data);
        // ob_clean();
        // header("Cache-Control: no-store");
        // header("Expires: 0");
        // header("Content-Type: application/octet-stream");
        // header('Content-Disposition: attachment; filename="'.$rows[0]->file_name.'"');
        // header("Content-Transfer-Encoding: binary");
        // header('Content-Length: '.filesize($rows[0]->file_path));
        // readfile($rows[0]->file_path);
        // exit();
    } 
}
