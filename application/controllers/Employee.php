<?php

class Employee extends CI_Controller {
    private static $_layout = '';

    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('logged_in'))
        {
            $this->session->set_flashdata('no_access', 'Sorry you are not allowed');
            redirect('user');
        }

        if ($this->session->userdata('employee_user_type') === 'Admin')
        {
            self::$_layout = 'admin_main';
        }
        else
        {
            self::$_layout = 'employee_main';
        }
    }

    public function index()
    {
        redirect('employee/transactions');
    }

    // CLIENT
    public function clients() {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['records'] = $this->employee_model->get_client_records();
        $data[self::$_layout] = 'users/employee/client/client_home';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function create_client()
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data[self::$_layout] = 'users/employee/client/client_create';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function save_client()
    {
        $this->form_validation->set_rules('client_name', 'Client Name', 'required');
        $this->form_validation->set_rules('client_username', 'Client Username', 'required');
        $this->form_validation->set_rules('password', 'Client Password', 'required');
        $this->form_validation->set_rules('mobile_num', 'Mobile Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('nationality', 'Nationality', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() === true)
        {
            $data = array(
                'client_name'       => $_POST['client_name'],
                'client_username'   => $_POST['client_username'],
                'client_password'   => $_POST['password'],
                'mobile_number'     => $_POST['mobile_num'],
                'email'             => $_POST['email'],
                'address'           => $_POST['address'],
                'nationality'       => $_POST['nationality'],
                'gender'            => $_POST['gender'],
                'archive'           => 0
            );

            if ($this->employee_model->save_client_record($data))
            {
                $this->session->set_flashdata('response', 'Record Save Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Save Record!');
            }

            redirect('employee/clients');
        }
        else
        {
            redirect('employee/clients');
        }
    }

    public function edit_client($record_id)
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['records'] = $this->employee_model->get_client_record($record_id);
        $data[self::$_layout] = 'users/employee/client/client_update';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function update_client($record_id)
    {
        $this->form_validation->set_rules('client_name', 'Client Name', 'required');
        $this->form_validation->set_rules('client_username', 'Client Username', 'required');
        $this->form_validation->set_rules('password', 'Client Password', 'required');
        $this->form_validation->set_rules('mobile_num', 'Mobile Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('nationality', 'Nationality', 'required');
        $this->form_validation->set_rules('gender', 'Gender', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() === true)
        {
            $data = array(
                'client_name'       => $_POST['client_name'],
                'client_username'   => $_POST['client_username'],
                'client_password'   => $_POST['password'],
                'mobile_number'     => $_POST['mobile_num'],
                'email'             => $_POST['email'],
                'address'           => $_POST['address'],
                'nationality'       => $_POST['nationality'],
                'gender'            => $_POST['gender']
            );

            if ($this->employee_model->update_client_record($record_id, $data))
            {
                $this->session->set_flashdata('response', 'Updated Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Update Record!');
            }

            redirect('employee/clients');
        }
        else
        {
            redirect('employee/clients');
        }
    }
    // END OF CLIENT

    // CO_BROKER
    public function co_brokers()
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['records'] = $this->employee_model->get_co_broker_records();
        $data[self::$_layout] = 'users/employee/co_broker/co_broker_home';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function create_co_broker()
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data[self::$_layout] = 'users/employee/co_broker/co_broker_create';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function save_co_broker()
    {
        $this->form_validation->set_rules('co_broker_name', 'Co-broker Name', 'required');
        $this->form_validation->set_rules('mobile_num', 'Mobile Number', 'required');
        $this->form_validation->set_rules('landline', 'Landline Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() === true)
        {
            $data = array(
                'co_broker_name'    => $_POST['co_broker_name'],
                'mobile_number'     => $_POST['mobile_num'],
                'landline'          => $_POST['landline'],
                'email'             => $_POST['email'],
                'address'           => $_POST['address'],
                'archive'           => 0
            );

            if ($this->employee_model->save_co_broker_record($data))
            {
                $this->session->set_flashdata('response', 'Record Save Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Save Record!');
            }

            redirect('employee/co_brokers');
        }
        else
        {
            redirect('employee/co_brokers');
        }
    }

    public function edit_co_broker($record_id)
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['records'] = $this->employee_model->get_co_broker_record($record_id);
        $data[self::$_layout] = 'users/employee/co_broker/co_broker_update';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function update_co_broker($record_id)
    {
        $this->form_validation->set_rules('co_broker_name', 'Co-broker Name', 'required');
        $this->form_validation->set_rules('mobile_num', 'Mobile Number', 'required');
        $this->form_validation->set_rules('landline', 'Landline Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() === true)
        {
            $data = array(
                'co_broker_name'    => $_POST['co_broker_name'],
                'mobile_number'     => $_POST['mobile_num'],
                'landline'          => $_POST['landline'],
                'email'             => $_POST['email'],
                'address'           => $_POST['address']
            );

            if ($this->employee_model->update_co_broker_record($record_id, $data))
            {
                $this->session->set_flashdata('response', 'Updated Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Update Record!');
            }

            redirect('employee/co_brokers');
        }
        else
        {
            redirect('employee/co_brokers');
        }
    }
    // END OF CO_BROKER

    // TRANSACTION
    public function transactions()
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['records'] = $this->employee_model->get_transaction_records();
        $data[self::$_layout] = 'users/employee/transaction/transaction_home';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function transaction($record_id)
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['records'] = $this->employee_model->get_transaction_record($record_id);
        $data[self::$_layout] = 'users/employee/transaction/transaction_profile';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function create_transaction()
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['employees'] = $this->employee_model->employee_filter();
        $data['clients'] = $this->employee_model->client_filter();
        $data['co_brokers'] = $this->employee_model->co_broker_filter();
        $data[self::$_layout] = 'users/employee/transaction/transaction_create';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function save_transaction()
    {
        $this->form_validation->set_rules('type_of_transaction', 'Type of transaction', 'required');
        $this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('client_seller_owner', 'Client', 'required');
        $this->form_validation->set_rules('floor_area', 'Floor Area', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('unit_number', 'Unit Number', 'required');
        $this->form_validation->set_rules('buyer_tenant', 'Buyer', 'required');
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('landline', 'Landline', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('sales_agent', 'Sales Agent', 'required');
        $this->form_validation->set_rules('co_broker', 'Co-broker', 'required');
        $this->form_validation->set_rules('building_project', 'Building/Project', 'required');
        //$this->form_validation->set_rules('userfile', 'Attachment', 'required');

        if ($_POST['type_of_transaction'] === "Pre-selling")
        {
            $this->form_validation->set_rules('date', 'Date', 'required');
            $this->form_validation->set_rules('developer', 'Developer', 'required');
            $this->form_validation->set_rules('contact_person', 'Contact Person', 'required');
        }
        else if ($_POST['type_of_transaction'] === "Resale")
        {
            $this->form_validation->set_rules('date', 'Date', 'required');
        }
        else if ($_POST['type_of_transaction'] === "Leasing" || $_POST['type_of_transaction'] === "Daily Rental")
        {
            $this->form_validation->set_rules('date_of_contract', 'Date of Contract', 'required');
            $this->form_validation->set_rules('end_of_contract', 'End of Contract', 'required');
            $this->form_validation->set_rules('association_dues', 'Association Dues', 'required');
            $this->form_validation->set_rules('remarks', 'Remarks', 'required');
        }

        if ($_POST['category'] === "House")
        {
            $this->form_validation->set_rules('lot_area', 'Lot Area', 'required');
        }

        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() === true)
        {
            $data = array(
                'type_of_transaction'   => $_POST['type_of_transaction'],
                'category'              => $_POST['category'],
                'name_of_seller_id'     => $_POST['client_seller_owner'],
                'name_of_buyer'         => $_POST['buyer_tenant'],
                'project'               => $_POST['building_project'],
                'floor_area'            => $_POST['floor_area'],
                'unit_number'           => $_POST['unit_number'],
                'price'                 => $_POST['price'],
                'mobile_number'         => $_POST['mobile_number'],
                'landline'              => $_POST['landline'],
                'email'                 => $_POST['email'],
                'address'               => $_POST['address'],
                'sales_person_id'       => $_POST['sales_agent'],
                'co_broker_id'          => $_POST['co_broker'],
                'archive'               => 0,
                'date'                  => null,
                'name_of_developer'     => '',
                'developer_agent'       => '',
                'date_of_contract'      => null,
                'end_of_contract'       => null,
                'association_dues'      => '',
                'remarks'               => '',
                'lot_area'              => ''
            );

            if ($_POST['type_of_transaction'] === "Pre-selling")
            {
                $data['date']               = $_POST['date'];
                $data['name_of_developer']  = $_POST['developer'];
                $data['developer_agent']    = $_POST['contact_person'];
            }
            else if ($_POST['type_of_transaction'] === "Resale")
            {
                $data['date'] = $_POST['date'];
            }
            else if ($_POST['type_of_transaction'] === "Leasing" || $_POST['type_of_transaction'] === "Daily Rental")
            {
                $data['date_of_contract']   = $_POST['date_of_contract'];
                $data['end_of_contract']    = $_POST['end_of_contract'];
                $data['association_dues']   = $_POST['association_dues'];
                $data['remarks']            = $_POST['remarks'];
            }

            if ($_POST['category'] === "House")
            {
                 $data['lot_area'] = $_POST['lot_area'];
            }

            if ($this->employee_model->save_transaction_record($data))
            {
        //              $this->save_activity_log($property_transaction_id, $this->session->userdata('employee_id'), 'Added by ' . $this->session->userdata('employee_name'));

                $this->session->set_flashdata('response', 'Record Save Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Save Record!');
            }

            redirect('employee/transactions');
        }
        else
        {
            redirect('employee/transactions');
        }
    }

    public function edit_transaction($record_id)
    {
        $data['notifications'] = $this->admin_model->get_unread_activity_records();
        $data['records'] = $this->employee_model->get_transaction_record($record_id);
        $data['employees'] = $this->employee_model->employee_filter();
        $data['clients'] = $this->employee_model->client_filter();
        $data['co_brokers'] = $this->employee_model->co_broker_filter();
        $data[self::$_layout] = 'users/employee/transaction/transaction_update';
        $this->load->view('layouts/' . self::$_layout, $data);
    }

    public function update_transaction($record_id)
    {
        $this->form_validation->set_rules('type_of_transaction', 'Type of transaction', 'required');
        $this->form_validation->set_rules('category', 'Category', 'required');
        $this->form_validation->set_rules('client_seller_owner', 'Client', 'required');
        $this->form_validation->set_rules('floor_area', 'Floor Area', 'required');
        $this->form_validation->set_rules('price', 'Price', 'required');
        $this->form_validation->set_rules('unit_number', 'Unit Number', 'required');
        $this->form_validation->set_rules('buyer_tenant', 'Buyer', 'required');
        $this->form_validation->set_rules('mobile_number', 'Mobile Number', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('landline', 'Landline', 'required');
        $this->form_validation->set_rules('address', 'Address', 'required');
        $this->form_validation->set_rules('sales_agent', 'Sales Agent', 'required');
        $this->form_validation->set_rules('co_broker', 'Co-broker', 'required');
        $this->form_validation->set_rules('building_project', 'Building/Project', 'required');
        //$this->form_validation->set_rules('userfile', 'Attachment', 'required');

        if ($_POST['type_of_transaction'] === "Pre-selling")
        {
            $this->form_validation->set_rules('date', 'Date', 'required');
            $this->form_validation->set_rules('developer', 'Developer', 'required');
            $this->form_validation->set_rules('contact_person', 'Contact Person', 'required');
        }
        else if ($_POST['type_of_transaction'] === "Resale")
        {
            $this->form_validation->set_rules('date', 'Date', 'required');
        }
        else if ($_POST['type_of_transaction'] === "Leasing" || $_POST['type_of_transaction'] === "Daily Rental")
        {
            $this->form_validation->set_rules('date_of_contract', 'Date of Contract', 'required');
            $this->form_validation->set_rules('end_of_contract', 'End of Contract', 'required');
            $this->form_validation->set_rules('association_dues', 'Association Dues', 'required');
            $this->form_validation->set_rules('remarks', 'Remarks', 'required');
        }

        if ($_POST['category'] === "House")
        {
            $this->form_validation->set_rules('lot_area', 'Lot Area', 'required');
        }

        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() === true)
        {
            $data = array(
                'type_of_transaction'   => $_POST['type_of_transaction'],
                'category'              => $_POST['category'],
                'name_of_seller_id'     => $_POST['client_seller_owner'],
                'name_of_buyer'         => $_POST['buyer_tenant'],
                'project'               => $_POST['building_project'],
                'floor_area'            => $_POST['floor_area'],
                'unit_number'           => $_POST['unit_number'],
                'price'                 => $_POST['price'],
                'mobile_number'         => $_POST['mobile_number'],
                'landline'              => $_POST['landline'],
                'email'                 => $_POST['email'],
                'address'               => $_POST['address'],
                'sales_person_id'       => $_POST['sales_agent'],
                'co_broker_id'          => $_POST['co_broker'],
                'archive'               => 0,
                'date'                  => null,
                'name_of_developer'     => '',
                'developer_agent'       => '',
                'date_of_contract'      => null,
                'end_of_contract'       => null,
                'association_dues'      => '',
                'remarks'               => '',
                'lot_area'              => ''
            );

            if ($_POST['type_of_transaction'] === "Pre-selling")
            {
                $data['date']               = $_POST['date'];
                $data['name_of_developer']  = $_POST['developer'];
                $data['developer_agent']    = $_POST['contact_person'];
            }
            else if ($_POST['type_of_transaction'] === "Resale")
            {
                $data['date'] = $_POST['date'];
            }
            else if ($_POST['type_of_transaction'] === "Leasing" || $_POST['type_of_transaction'] === "Daily Rental")
            {
                $data['date_of_contract']   = $_POST['date_of_contract'];
                $data['end_of_contract']    = $_POST['end_of_contract'];
                $data['association_dues']   = $_POST['association_dues'];
                $data['remarks']            = $_POST['remarks'];
            }

            if ($_POST['category'] === "House")
            {
                $data['lot_area'] = $_POST['lot_area'];
            }

            if ($this->employee_model->update_transaction_record($record_id, $data))
            {
                $this->save_activity_log($record_id, $this->session->userdata('employee_id'), 'Edited by ' . $this->session->userdata('employee_name'));

                $this->session->set_flashdata('response', 'Updated Successfully.');
            }
            else
            {
                $this->session->set_flashdata('response', 'Failed to Update Record!');
            }

            redirect('employee/transactions');
        }
        else
        {
            redirect('employee/transactions');
        }
    }
    // END OF TRANSACTION

    // ACTIVITY LOG
    public function save_activity_log($property_transaction_id, $employee_id, $activity)
    {
        $data = array(
            'property_transaction_id'   => $property_transaction_id,
            'employee_id'               => $employee_id,
            'activity_log_date'         => date('Y-m-d H:i:s'),
            'activity'                  => $activity
        );

        $this->employee_model->save_activity_log_record($data);

        return true;
    }
    // END OF ACTIVITY LOG
}