<?php

class Employee_model extends CI_Model {

    // CLIENT
    public function get_client_records()
    {
        $query = $this->db->get_where('client', array('archive' => 0));
        return $query->result();
    }

    public function get_client_record($record_id)
    {
        $query = $this->db->get_where('client', array('client_id' => $record_id));

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function save_client_record($data)
    {
        return $this->db->insert('client', $data);
    }

    public function update_client_record($record_id, $data)
    {
         return $this->db->where('client_id', $record_id)->update('client', $data);
    }
    // END OF CLIENT

    // CO_BROKER
    public function get_co_broker_records()
    {
        $query = $this->db->get_where('co_broker', array('archive' => 0));
        return $query->result();
    }

    public function get_co_broker_record($record_id)
    {
        $query = $this->db->get_where('co_broker', array('co_broker_id' => $record_id));

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function save_co_broker_record($data)
    {
        return $this->db->insert('co_broker', $data);
    }

    public function update_co_broker_record($record_id, $data)
    {
         return $this->db->where('co_broker_id', $record_id)->update('co_broker', $data);
    }
    // END OF CO_BROKER

    // PROPERTY TRANSACTION
    public function get_transaction_records()
    {
        $query = $this->db->get_where('property_transaction', array('archive' => 0));
        return $query->result();
    }

    public function get_transaction_record($record_id)
    {
        $query = $this->db->get_where('property_transaction', array('property_transaction_id' => $record_id));

        if ($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function save_transaction_record($data)
    {
        return $this->db->insert('property_transaction', $data);
    }

    public function update_transaction_record($record_id, $data)
    {
         return $this->db->where('property_transaction_id', $record_id)->update('property_transaction', $data);
    }

    public function employee_filter()
    {
        $query = $this->db->query('SELECT employee_id, employee_name FROM employee WHERE archive = 0');
        return $query->result();
    }

    public function client_filter()
    {
        $query = $this->db->query('SELECT client_id, client_name FROM client WHERE archive = 0');
        return $query->result();
    }

    public function co_broker_filter()
    {
        $query = $this->db->query('SELECT co_broker_id, co_broker_name FROM co_broker WHERE archive = 0');
        return $query->result();
    }
    // END OF PROPERTY TRANSACTION

	// ACTIVITY LOG
    public function save_activity_log_record($data)
	{
		return $this->db->insert('activity_log', $data);
	}
    // END OF ACTIVITY LOG


}

