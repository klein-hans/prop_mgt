<?php

class Property_model extends CI_Model {

    public function get_all_property_transaction($user_id) {
//        $this->db->where([
//            'user_id' => $user_id
//        ]);

        $query = $this->db->get('property_transaction');

        return $query->result();
    }

    public function create_property_transaction($data) {
        return $this->db->insert('property_transaction', $data);
    }

    public function update_property_transaction($data, $id) {
        $this->db->where([
            'property_transaction_id' => $id
        ]);

        $this->db->update('users', $data);
    }

    public function delete_property_transaction($id) {
        $this->db->where([
            'property_transaction_id' => $id
        ]);

        $this->db->delete('property_transaction');
    }
}