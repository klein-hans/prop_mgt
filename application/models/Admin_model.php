<?php

class Admin_model extends CI_Model {

    // EMPLOYEE
    public function get_employee_records()
    {
        $query = $this->db->get_where('employee', array('archive' => 0));
        return $query->result();
    }

    public function get_employee_record($record_id)
    {
        $query = $this->db->get_where('employee', array('employee_id' => $record_id));

        if($query->num_rows() > 0)
        {
            return $query->row();
        }
    }

    public function save_employee_record($data)
    {
        return $this->db->insert('employee', $data);
    }

    public function update_employee_record($record_id, $data)
    {
        return $this->db->where('employee_id', $record_id)->update('employee', $data);
    }

    public function delete_employee_record($record_id, $data)
    {
        //return $this->db->delete('employee', array('employee_id' => $record_id));
        return $this->db->where('employee_id', $record_id)->update('employee', $data);
    }
    // END OF EMPLOYEE

    // CLIENT
    public function delete_client_record($record_id, $data)
    {
//        return $this->db->delete('client', array('client_id' => $record_id));
        return $this->db->where('client_id', $record_id)->update('client', $data);
    }
    // END OF CLIENT

    // CO_BROKER
    public function delete_co_broker_record($record_id, $data)
    {
//        return $this->db->delete('co_broker', array('co_broker_id' => $record_id));
        return $this->db->where('co_broker_id', $record_id)->update('co_broker', $data);
    }
    // END OF CO_BROKER

    // PROPERTY TRANSACTION
    public function delete_transaction_record($record_id, $data)
    {
//        return $this->db->delete('property_transaction', array('property_transaction_id' => $record_id));
        return $this->db->where('property_transaction_id', $record_id)->update('property_transaction', $data);
    }
    // END OF PROPERTY TRANSACTION

    // ACTIVITY LOG
    public function get_activity_log_records()
    {
        $query = $this->db->get('activity_log');
        return $query->result();
    }

    public function get_unread_activity_records()
    {
        $query = $this->db->get_where('activity_log', array('activity_read' => 0));
        return $query->result();
    }
    // END OF ACTIVITY LOG

    /*====================
        File Uploading
    ====================*/
    public function file_uploading($data){
        return $this->db->insert('listings', $data);
    }

    /*====================
            Listings
    ====================*/
    public function get_listings_records($data){
        $where = "";
        if($data != null){
            $last_x = end($data);
            $last_y = end($last_x);

            foreach (array_keys($data) as $name ) {
                foreach (array_keys($data[$name]) as $key) {
                    if($name == "bedrooms"){
                        $where .= "br.name"." = '".$data[$name][$key]."' ";
                    }else{
                        $where .= $name." = '".$data[$name][$key]."' ";    
                    }
                    
                    
                    if($data[$name][$key] != $last_y){
                        $where .= "OR ";
                    }
                }
            }   
            $where .= "AND ";
        }
        
        $where .= "l.archive = 0";
        $sql = "SELECT l.*, br.name as bedroom_name, p.name as parking_name 
                FROM listings l
                INNER JOIN listings_bedrooms br ON l.bedroom_id = br.id  
                INNER JOIN listings_parking p ON l.parking_id = p.id 
                WHERE $where";
                    // var_dump($sql);
                    // exit();
        $query = $this->db->query($sql);
        return $query->result();    
    }

    public function get_listings_record($id){
        $query = "SELECT l.*, l.id as listings_id, br.name as bedroom_name, p.name as parking_name 
                FROM listings l
                INNER JOIN listings_bedrooms br ON l.bedroom_id = br.id  
                INNER JOIN listings_parking p ON l.parking_id = p.id
                WHERE l.archive = 0 AND l.id = $id";
                
        $result = $this->db->query($query);
        $row = $result->row();
        return $row;
    }

    public function get_listings_photos($id){
        $query = "SELECT *
                FROM listings_photos photos
                WHERE photos.listings_id = $id";
        $rows = $this->db->query($query);
        return $rows->result();
    }

    public function get_listings_files($id){
        $query = "SELECT *
                FROM  listings_files files
                WHERE files.listings_id = $id";                
        $rows = $this->db->query($query);
        return $rows->result();
    }   

    public function add_listings_record($data)
    {
        $this->db->insert('listings', $data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }

    public function delete_listings_record($id, $data){
        return $this->db->where('id', $id)->update('listings', $data);
    }

    public function edit_listings_record($data){
        $this->db->where('id', $data['id'])->update('listings', $data);
        return $data['id'];
    }

    public function upload(){
        $this->db->insert('file_table',array('file_name' => $fileName));
    }

    /*====================
           Bedrooms
    ====================*/
    public function get_bedrooms(){
        $sql = "SELECT *,name as bedroom_name 
        FROM listings_bedrooms WHERE archive = 0";
        $query = $this->db->query($sql);
        return $query->result();    
    }

    public function get_bedroom_record($id){
        $query = "SELECT *,name as bedroom_name 
                FROM listings_bedrooms
                WHERE archive = 0 AND id = $id";
        $result = $this->db->query($query);
        $row = $result->row();
        return $row;
    }

    /*====================
            Parking
    ====================*/
    public function get_parking(){
        $sql = "SELECT *, name as parking_name 
        FROM listings_parking  WHERE archive = 0";
        $query = $this->db->query($sql);
        return $query->result();    
    }
    
    public function get_parking_record($id){
        $query = "SELECT *,name as parking_name 
                FROM listings_parking
                WHERE archive = 0 AND id = $id";
        $result = $this->db->query($query);
        $row = $result->row();
        return $row;
    }

    /*====================
            CRUD
    ====================*/
    public function get_record($where, $table)
    {
        $query = $this->db->get_where($table, $where);
        return $query->result();
    }
    public function get_records($table)
    {
        $query = $this->db->get($table);  
        return $query->result();
    }
    public function add_record($data, $table)
    {
        return $this->db->insert($table, $data);
    }

    public function edit_record($data, $table)
    {
        return $this->db->where('id', $data['id'])->update($table, $data);
    }

    public function delete_record($table, $id, $data)
    {
        return $this->db->where('id', $id)->update($table, $data);
    }

    public function delete_permanent($table, $id)
    {
        $query = $this->db->get_where($table, array('id' => $id));
        $this->db->delete($table, array('id' => $id));   
        return $query->result();
    }

    /*====================
        Upload Files
    ====================*/
    public function add_files($data, $table){
        return $this->db->insert($table, $data);
    }
}
