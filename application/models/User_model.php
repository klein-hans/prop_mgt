<?php

class User_model extends CI_Model {

    public function login_user($username, $password, $table_name)
    {
        $this->db->where([
            $table_name.'_username' => $username,
            $table_name.'_password' => $password
        ]);

        $query = $this->db->get($table_name);

        if ($query->num_rows() === 1) {
//            return $query->row(0)->$table_name.'_'.id;
            return $query->result();
        } else {
            return false;
        }
    }

    public function profile()
    {
        $this->db->select('*');
        $this->db->from('employee');
        $query = $this->db->get();

        //$user = $query->row();

        if ($user = $query->row())
        {
            //set session variables
            $_SESSION['user_logged'] = TRUE;
            $_SESSION['username'] = $user->employee_username;
            $_SESSION['name'] = $user->employee_name;
            $_SESSION['position'] = $user->position;
            $_SESSION['mobile'] = $user->mobile_number;
            $_SESSION['email'] = $user->email;
            $_SESSION['usertype'] = $user->user_type;

             $this->load->view('layouts/main');
            $this->load->view('users/profile');
        }
    }
}