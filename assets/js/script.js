
$(document).ready(function() {

//    var $date               = $('#date');
//    var $developer          = $('#developer');
//    var $lot_area           = $('#lot_area');
//    var $contact_person     = $('#contact_person');
//    var $date_of_contract   = $('#date_of_contract');
//    var $end_of_contract    = $('#end_of_contract');
//    var $buyer_tenant       = $('#buyer_tenant');
//    var $association_dues   = $('#association_dues');
//    var $remarks            = $('#remarks');

    var type_of_transaction = $('#type_of_transaction');
    var category            = $('#category');

    var client_seller_owner = $('.client_seller_owner');
    var buyer_tenant        = $('.buyer_tenant');
    var buyer_tenant_mn     = $('.buyer_tenant_mn');
    var buyer_tenant_l      = $('.buyer_tenant_l');
    var buyer_tenant_a      = $('.buyer_tenant_a');

    var pre_selling         = $('.pre_selling');
    var resale              = $('.resale');
    var leasing             = $('.leasing');
    var daily_rental        = $('.daily_rental');
    var house               = $('.house');
    var all                 = $('.all');

    var floor_area          = $('.floor_area');

    function hide_all(){
        pre_selling.attr('hidden', 'hidden');
        resale.attr('hidden', 'hidden');
        leasing.attr('hidden', 'hidden');
        daily_rental.attr('hidden', 'hidden');
        house.attr('hidden', 'hidden');
        all.attr('hidden', 'hidden');
    }

    function check_category()
    {
        if (category.val() === 'House')
        {
            house.removeAttr('hidden');
            floor_area.removeClass('col-12');
            floor_area.addClass('col-6');
        }
        else
        {
            house.attr('hidden', 'hidden');
            floor_area.removeClass('col-6');
            floor_area.addClass('col-12');
        }
    }

    type_of_transaction.change(function()
    {
        if (type_of_transaction.val() === 'Pre-selling')
        {
            hide_all();
            pre_selling.removeAttr('hidden');
            all.removeAttr('hidden');
            check_category();
        }
        else if (type_of_transaction.val() === 'Resale')
        {
            hide_all();
            resale.removeAttr('hidden');
            all.removeAttr('hidden');
            check_category();
        }
        else if (type_of_transaction.val() === 'Leasing')
        {
            hide_all();
            leasing.removeAttr('hidden');
            all.removeAttr('hidden');
            check_category();
        }
        else if (type_of_transaction.val() === 'Daily Rental')
        {
            hide_all();
            daily_rental.removeAttr('hidden');
            all.removeAttr('hidden');
            check_category();
        }
        else
        {
            hide_all();
        }
    }).trigger('change');

    category.change(function()
    {
        check_category();
    }).trigger('change');
});
//    $type_of_transaction.change(function()
//    {
//        if ($type_of_transaction.val() === 'Pre-selling')
//        {
//            $developer.removeAttr('disabled');
//            $contact_person.removeAttr('disabled');
//            $date.removeAttr('disabled');
//            $buyer_tenant.removeAttr('disabled');
//        }
//        else
//        {
//            $developer.attr('disabled', 'disabled').val('');
//            $contact_person.attr('disabled', 'disabled').val('');
//            $date.attr('disabled', 'disabled').val('');
//            $buyer_tenant.attr('disabled', 'disabled').val('');
//        }
//    }).trigger('change');
//
//    $category.change(function()
//    {
//        if ($category.val() === 'House')
//        {
//            $lot_area.removeAttr('disabled');
//        }
//        else
//        {
//            $lot_area.attr('disabled', 'disabled').val('');
//        }
//    }).trigger('change');
//
//    $type_of_transaction.change(function()
//    {
//        if ($type_of_transaction.val() === 'Resale')
//        {
//            $date.removeAttr('disabled');
//            $buyer_tenant.removeAttr('disabled');
//        }
//    }).trigger('change');
//
//    $type_of_transaction.change(function()
//    {
//        if ($type_of_transaction.val() === 'Leasing') {
//            $date_of_contract.removeAttr('disabled');
//            $end_of_contract.removeAttr('disabled');
//            $association_dues.removeAttr('disabled');
//            $remarks.removeAttr('disabled');
//        }
//        else
//        {
//            $date_of_contract.attr('disabled', 'disabled').val('');
//            $end_of_contract.attr('disabled', 'disabled').val('');
//            $association_dues.attr('disabled', 'disabled').val('');
//            $remarks.attr('disabled', 'disabled').val('');
//        }
//    }).trigger('change');


/*========================
    Copy to Clipboard
=======================*/
function copy(){
    var text= document.getElementById('text');
    var range = document.createRange();
    range.selectNode(text);
    window.getSelection().addRange(range);
    document.execCommand('copy');
}

/*====================
        Modal
====================*/  
var modal = document.getElementById('delete_modal');
var btn_delete = document.getElementsByClassName("btn_delete");
btn_delete.onclick = function() {
    modal.style.display = "block";
}

/*====================
        DatePicker
====================*/  
$( function() {
    $( "#datepicker" ).datepicker();
});

$( "#availability" ).change(function(){
    var availability = $( "#availability" ).val();    
    if(availability == "Will be Available on"){
        $('.div_listing_datepicker').css('display', 'block');
    }else{
        $('.div_listing_datepicker').css('display', 'none');
    }
});

    
/*====================
    Data Tables
====================*/  
$('#employee-table').DataTable();
$('#client-table').DataTable({
    "scrollX": true
});
$('#co-broker-table').DataTable();
$('#transaction-table').DataTable({
    "scrollX": true
});
$('#listings_table').dataTable({
    "pageLength": 20,
     "lengthMenu": [20, 30, 40, 50],
    "dom": "<'pull-left'f><'pull-right'B>" +
            "t" +
            "<'pull-left'l><'pull-right'ps>",
    "buttons"  : [
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
        ],
    responsive: {
        details: {
            type: 'column'
        }
    }
});
$('#contacts_table').dataTable({
    "pageLength": 5,
    "lengthMenu": [5, 10, 25, 50],
    "dom": "<'pull-left'f><'pull-right'B>" +
            "t" +
            "<'pull-left'l><'pull-right'ps>",
    "buttons"  :
                {
                extend: 'copy',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
          ]
});


$('#bedrooms_table').dataTable({
    "pageLength": 5,
    "lengthMenu": [5, 10, 25, 50],
    "dom": "<'pull-left'f>" +
            "t" +
            "<'pull-left'l><'pull-right'ps>"
});
$('#parking_table').dataTable({
    "pageLength": 5,
    "lengthMenu": [5, 10, 25, 50],
    "dom": "<'pull-left'f>" +
            "t" +
            "<'pull-left'l><'pull-right'ps>"
});


/*====================
    Hides Lot Area
====================*/  
$( "#property_type" ).change(function(){
    var property_type = $( "#property_type" ).val();    
    if(property_type == "Condo"){
        $('#lot_area').css('display', 'none');
        $('#lot_area_label').css('display', 'none');
    }else{
        $('#lot_area').css('display', 'block');
        $('#lot_area_label').css('display', 'block');
    }
});

/*====================
      Validation
====================*/  
function characters_length(val, e){
  var key = val + String.fromCharCode(!e.charCode ? e.which : e.charCode);
  if((key.length > 35 )){
    e.preventDefault();
  }
}

function numeric(event){
  var regex = new RegExp(/^\+639\d*$/);
  var key = value + String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (!regex.test(key)) {
     event.preventDefault();
     return false;
  }
}
$('#property_name').keypress(function(e){ 
  var val = $('#property_name').val();
  characters_length(val,e); 
});
$('#unit_number').keypress(function(e){ 
  var val = $('#unit_number').val();
  characters_length(val,e);
});
$('#floor_area').keypress(function(e){ 
  var val = $('#floor_area').val();
  characters_length(val,e);
});
$('#lot_area').keypress(function(e){ 
  var val = $('#lot_area').val();
  characters_length(val,e);
});
$('#remarks').keypress(function(e){ 
  var val = $('#remarks').val();
  characters_length(val,e);
});
$('#owner_name').keypress(function(e){ 
  var val = $('#owner_name').val();
  characters_length(e);
});
$('#email_address').keypress(function(e){ 
  var val = $('#email_address').val();
  characters_length(val,e); 
});

$('#price').keypress(function(event){
  var val = $('#price').val();
  var re = /^[0-9]*$/;
  var key = val + String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (!re.test(key)){
     event.preventDefault();
  }
  if((key.length > 15 )){
    event.preventDefault();
  }
});

$('#mobile_number').keypress(function (event) {
  var val = $('#mobile_number').val();
  var regex = new RegExp(/^\+639\d*$/);
  var key = val + String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (!regex.test(key)){
     event.preventDefault();
     return false;
  }
  
  if((key.length > 13 )){
    event.preventDefault();
  }
});

$('#mobile_number').keydown(function (event) {
  var val = $('#mobile_number').val();
  var key = val + String.fromCharCode(!event.charCode ? event.which : event.charCode);
  var keycode = event.keyCode;
  console.log("test");
  if ((event.keyCode === 8) && (key.length < 6)){
    console.log("test");
    event.preventDefault();
  }
});

$('#telephone_number').keypress(function(event){
  var val = $('#telephone_number').val();
  var re = /^[0-9]*$/;
  var key = val + String.fromCharCode(!event.charCode ? event.which : event.charCode);
  if (!re.test(key)){
     event.preventDefault();
  }
  if((key.length > 7 )){
    event.preventDefault();
  }
});


/*====================
    Delete Button
====================*/  
var tbl = "";
var id = "";
var listings_id = "";
var url = "";
$('.btn_delete').click(function(e){
    id = $(this).data('id');
    listings_id = $(this).data('listings_id');
    url = $(this).data('url');
    tbl = $(this).data('table');  
    $('#anchor_delete').prop("href", url+listings_id+id+tbl);
}); 

/*====================
    File Uploading
====================*/  
$(document).on('change', ':file', function() {
var input = $(this),
    numFiles = input.get(0).files ? input.get(0).files.length : 1,
    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
input.trigger('fileselect', [numFiles, label]);
});

// We can watch for our custom `fileselect` event like this
$(document).ready( function() {
  $(':file').on('fileselect', function(event, numFiles, label) {

      var input = $(this).parents('.input-group').find(':text'),
          log = numFiles > 1 ? numFiles + ' files selected' : label;

      if( input.length ) {
          input.val(log);
      } else {
          if( log ) alert(log);
      }

  });
});

/*====================
    Image Gallery
====================*/  
var slideIndex = 1; 
showSlides(slideIndex);

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("main_image");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  slides[slideIndex-1].style.display = "block";
}


// Open the Modal
function openModal() {
  document.getElementById('lightbox').style.display = "block";
}

// Close the Modal
function closeModal() {
  document.getElementById('lightbox').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);


// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("main_image");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace("active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}





